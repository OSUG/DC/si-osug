from django.contrib import admin

from .models import (Config, History, ActionRecord, Machine, Package, Platform,
                     PythonPackage, SSHKey, SSHKeyType, Team, Template, User,
                     UserGroup, Vlan, VMDatastore, VMDisk, VMGuestOs)


@admin.register(History)
class History_Admin(admin.ModelAdmin):
    list_display = ('timestamp', 'username', 'model', 'operation',)
    ordering = ['-timestamp']
    readonly_fields = ('timestamp',)


@admin.register(ActionRecord)
class ActionRecord_Admin(admin.ModelAdmin):
    list_display = ('id', 'create_timestamp', 'verb', 'state', 'exec_timestamp',)
    ordering = ['-id']
    list_display_links = ('create_timestamp',)
    readonly_fields = ('create_timestamp',)


@admin.register(Config)
class Config_Admin(admin.ModelAdmin):
    list_display = ('key', 'value',)


@admin.register(User)
class User_Admin(admin.ModelAdmin):
    list_display = ('username', 'unix_uid', "admin_flag")


@admin.register(UserGroup)
class UserGroup_Admin(admin.ModelAdmin):
    list_display = ('groupname', 'unix_gid',)


@admin.register(SSHKeyType)
class SSHKeyType_Admin(admin.ModelAdmin):
    list_display = ('key_type',)


@admin.register(SSHKey)
class SSHKey_Admin(admin.ModelAdmin):
    list_display = ('key_email', 'key_type',)


@admin.register(Team)
class Team_Admin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Platform)
class Platform_Admin(admin.ModelAdmin):
    pass


@admin.register(Machine)
class Machine_Admin(admin.ModelAdmin):
    list_display = ('platform', 'vm_template', 'vm_name', 'net_hostname',)
    list_display_links = ('vm_name',)


@admin.register(Template)
class Template_Admin(admin.ModelAdmin):
    list_display = ('name', 'guest_id',)


@admin.register(VMGuestOs)
class VMGuestOs_admin(admin.ModelAdmin):
    list_display = ('name', 'description',)

    
@admin.register(VMDatastore)
class VMDatastore_admin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ['name']

@admin.register(VMDisk)
class VMDisk_admin(admin.ModelAdmin):
    list_display = ('vm', 'label', 'size_gb', 'datastore', 'disk_id', 'uuid')
    ordering = ['vm', 'disk_id', 'datastore', 'index']

@admin.register(Vlan)
class Vlan_admin(admin.ModelAdmin):
    list_display = ('name', 'active')


@admin.register(Package)
class Package_Admin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(PythonPackage)
class PythonPackage_Admin(admin.ModelAdmin):
    list_display = ('name',)
