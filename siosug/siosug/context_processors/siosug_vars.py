# -*- config: utf-8 -*-

"""
SiOsug configuration variables context processor
"""

from django.conf import settings

def siosug_vars(request):   # pylint: disable=W0613
    """
    returns application specific configuration variables
    """
    return {
        'APP_NAME': settings.APP_NAME
    }
