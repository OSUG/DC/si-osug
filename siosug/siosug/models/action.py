
"""
Model for an action record
"""

import os
import subprocess

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models

from siosug.models.machine import Machine


class ActionRecord(models.Model):
    """
    Action record
    stores actions to be done by the action engine
    """
    AS_TODO = 0
    AS_DONE = 1
    AS_ERROR = 2
    AS_SKIPPED = 3
    ACTION_STATES = (
        (AS_TODO, 'To Do',),
        (AS_DONE, 'Done',),
        (AS_ERROR, 'Error',),
        (AS_SKIPPED, 'Skipped',),
    )

    AR_CREATE_VM = "CREATE_VM"
    AR_UPDATE_VM = "UPDATE_VM"

    create_timestamp = models.DateTimeField(unique=True, auto_now_add=True)
    verb = models.CharField(max_length=32, null=False, blank=False)
    params = JSONField(null=False, blank=False, default=dict)
    state = models.SmallIntegerField(choices=ACTION_STATES, default=AS_TODO)
    exec_timestamp = models.DateTimeField(null=True, blank=True)
    logs = models.TextField(null=True, blank=True)
    errors = models.TextField(null=True, blank=True)

    def run(self):
        if self.state == self.AS_TODO:
            func = getattr(self, 'run_'+self.verb)
            if callable(func):
                func()

    def _run_command(self, path, command):
        print(path, command)
        environment = os.environ
        if 'ANSIBLE_STDOUT_CALLBACK' not in environment:
            environment['ANSIBLE_STDOUT_CALLBACK'] = 'debug'
        result = subprocess.run(command, cwd=path, env=environment, capture_output=True)

        return result

    def _create_or_update_vm(self):
        #obtain vm 
        vm_id = self.params.get('vm_id', None)
        if not vm_id:
            self.state = self.AS_SKIPPED
            self.log = "missing 'vm_id' parameter"
            self.save()
            return
        from siosug.models import Machine
        virtual_machine = Machine.objects.get(id=vm_id)
        path = settings.ANSIBLE_DIR
        command = ['env', 'ansible-playbook', '-v', '-l', virtual_machine.net_hostname, 'gen-platform.yml']
        result = self._run_command(path, command)
        print(result.returncode)
        print("========== STDOUT")
        print(result.stdout)
        self.logs = result.stdout.decode('utf-8')
        print("========== STDERR")
        print(result.stderr)
        self.errors = result.stderr.decode('utf-8')
        self.save()


    def run_CREATE_VM(self):
        print("running CREATE_VM (params %s)"%(str(self.params)))
        self._create_or_update_vm()

    def run_UPDATE_VM(self):
        print("running UPDATE_VM (params %s)"%(str(self.params)))
        self._create_or_update_vm()
