# -*- encoding: utf8 -*-

import logging

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.db.utils import IntegrityError
from passlib.hash import ldap_pbkdf2_sha512

class SiOsugUserManager(BaseUserManager):
    def create_user(self, username):
        print("creating user with ", username)
        return None

class User(models.Model):
#class User(AbstractBaseUser):
    """
    a system user, modeled on unix user database info
    """
    uid = models.IntegerField(primary_key=True, unique=True, null=False, blank=False)
    username = models.CharField(unique=True, max_length=32, null=False,
                                blank=False)
    # this is 256 long, may need to be longer,
    # but should do for most hashing systems
    unix_hash = models.CharField(max_length=256, null=True, blank=True)
    ssh_pubkeys = models.ManyToManyField('SSHKey', related_name='user', blank=True)
    last_login = models.DateTimeField(blank=True, null=True)
    admin_flag = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

   # objects = SiOsugUserManager()

    def __str__(self):
        return self.username

    def get_ssh_keys(self):
        keys = []
        for k in self.ssh_pubkeys.all():
            print(k)
            keys.append(k.get_ssh_key_line())
        print("user ", self.username, " keys ", keys)
        return keys

    # ----------------------------------------------------------------------------------------
    # User authentication stuff
    # TODO: take a good hard look at this...
    # copied from 
    # https://github.com/django/django/blob/master/django/contrib/auth/base_user.py

    def get_username(self):
        """Return the username for this User."""
        return getattr(self, self.USERNAME_FIELD)  

    def natural_key(self):
        return (self.get_username(),)

    # ----
    #
    #
    #
    @property
    def unix_uid(self):
        return self.uid

    @unix_uid.setter
    def unix_uid(self, value):
        self.uid = value

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_staff(self):
        return True

    @property
    def is_admin(self):
        return self.admin_flag

    @property
    def is_superuser(self):
        return True

    def has_perm(self, perm, obj=None):
        logger = logging.getLogger("OSUG."+self.__class__.__name__)
        logger.info("User.has_perm %s %s", str(perm), str(obj))
        return self.is_admin

    def has_module_perms(self, app_label):
        logger = logging.getLogger("OSUG."+self.__class__.__name__)
        logger.info("User.has_module_perm %s", str(app_label))
        return self.is_admin

    def is_password_valid(self, password):
        """
        Receives password as cleartext, checks it against the hashed stored value
        """
        print("checking password from the django database")
        if self.unix_hash is not None and self.unix_hash != '':
            if ldap_pbkdf2_sha512.verify(password, self.unix_hash):
                print('password matches')
                return True
            print("password doesn't match")
        else:
            print('unix_hash field is empty')
        return False

    def set_password(self, password):
        # check if has is set and password valid
        if self.unix_hash is not None and self.unix_hash != '':
            if ldap_pbkdf2_sha512.verify(password, self.unix_hash):
                print('password matches, skipping')
                return
            print("password doesn't match, update")
        else:
            print('unix_hash field is empty')
        # password doesn't match... update
        passwd_hash = ldap_pbkdf2_sha512.hash(password)
        print(passwd_hash)
        self.unix_hash = passwd_hash
        try:
            self.save()
        except IntegrityError as e:
            logger = logging.getLogger("OSUG."+self.__class__.__name__)
            logger.critical(e)

# ----------------------------------------------------------------------------------------
# User authentication stuff


class SSHKeyType(models.Model):
    key_type = models.CharField(max_length=32, null=False, blank=False)

    def __str__(self):
        return self.key_type


class SSHKey(models.Model):
    key_type = models.ForeignKey('SSHKeyType', on_delete=models.DO_NOTHING,
                                 null=False, blank=False)
    key_data = models.CharField(max_length=2048, null=False, blank=False)
    key_email = models.CharField(max_length=254, null=False, blank=False)

    def __str__(self):
        return '%s (%s)' % (self.key_email, self.key_type)

    def get_ssh_key_line(self):
        return '%s %s %s' % (self.key_type.key_type,
                             self.key_data, self.key_email)
