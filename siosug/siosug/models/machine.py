# -*- encoding: utf8 -*-

"""
Model for a Virtual Machine
"""

from django.db import models

from .platform import Platform
from .vmware import Template, VMDatastore, Vlan

class Machine(models.Model):
    """
    a machine is part of a platform
        * has a parent platform
    """
    platform = models.ForeignKey(Platform, on_delete=models.DO_NOTHING,
                                 related_name='machines', null=True)

    # VMware

    vm_template = models.ForeignKey(Template, on_delete=models.DO_NOTHING,
                                    null=True, blank=True)
    vm_folder = models.CharField(max_length=128, null=True, blank=True)
    vm_datastore = models.ForeignKey(VMDatastore,
                                     on_delete=models.DO_NOTHING,
                                     null=True, blank=True)
    vm_name = models.CharField(max_length=64, null=False, blank=False,
                               default='<new-vm>')  # obligatoire

    # hardware virtuel

    hw_cpus = models.IntegerField(null=True, blank=True)
    hw_memory = models.IntegerField(null=True, blank=True)  # Megabytes
    hw_root = models.IntegerField(null=True, blank=True)  # Gigabytes

    # network configuration

    net_vlan = models.ForeignKey(Vlan, on_delete=models.DO_NOTHING, null=True, blank=True)
    # net_vlan_text = models.CharField(max_length=128, null=True, blank=True)
    net_hostname = models.CharField(max_length=64, null=False, blank=False,
                                    default='<new-vm>')
    net_names = models.CharField(max_length=256, null=True, blank=True)
    net_search_domains = models.CharField(max_length=256, null=True,
                                          blank=True)
    net_ip = models.GenericIPAddressField(null=True, blank=True)
    net_pfx_len = models.IntegerField(null=True, blank=True)
    net_gw = models.GenericIPAddressField(null=True, blank=True)

    # user for local administration

    admin_user = models.CharField(max_length=32, null=True, blank=True)

    # packages
    pkg_install = models.ManyToManyField('Package', blank=True)
    pkg_install_pip = models.ManyToManyField('PythonPackage', blank=True)

    last_applied = models.DateTimeField(null=True, blank=True)
    last_update = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '%s / %s' % (self.platform, self.vm_name)

    # ----
    # Accessor functions

    def get_vmware_name(self):
        if self.vm_name is None or self.vm_name == '':
            return None
        return self.vm_name

    def get_vmware_template(self):
        if self.vm_template is None:
            return self.platform.get_vmware_template()
        return self.vm_template.name

    def get_vmware_guest_id(self):
        if self.vm_template is None:
            return self.platform.get_vmware_guest_id()
        return self.vm_template.guest_id.name

    def get_vmware_folder(self):
        if self.vm_folder is None or self.vm_folder == '':
            return self.platform.get_vmware_folder()
        return self.vm_folder

    def get_vmware_datastore(self):
        if self.vm_datastore is None:
            return self.platform.get_vmware_datastore()
        return self.vm_datastore.name

    def get_hw_cpus(self):
        if self.hw_cpus is None or self.hw_cpus == 0:
            return self.platform.get_hw_cpus()
        return self.hw_cpus

    def get_hw_ram(self):
        if self.hw_memory is None or self.hw_memory == 0:
            return self.platform.get_hw_ram()
        return self.hw_memory

    def get_hw_root_size(self):
        if self.hw_root is None or self.hw_root == 0:
            return self.platform.get_hw_root_size()
        return self.hw_root

    def get_hw_root_size_as_percent_vg(self):
        from .config import Config
        root_size = self.get_hw_root_size()
        # should try/catch lots of errors possible here
        vg_default_size = int(Config.objects.get(key='VG_DEFAULT_SIZE').value)
        if not root_size:
            root_size = vg_default_size
        percent = int(root_size*100/vg_default_size)
        if percent > 100:
            percent = 100
        return '%s%%VG' % (str(percent))

    def get_net_vlan(self):
        if self.net_vlan is None or self.net_vlan == '':
            return self.platform.get_net_vlan()
        return self.net_vlan.name

    def get_net_hostname(self):
        if self.net_hostname is None or self.net_hostname == '':
            return None
        return self.net_hostname

    def get_net_names(self):
        if self.net_names is None or self.net_names == '':
            return None
        return self.net_names

    def get_net_search_domains(self):
        if self.net_search_domains is None or self.net_search_domains == '':
            return self.platform.get_net_search_domains()
        return self.net_search_domains

    def get_net_ip(self):
        if self.net_ip is None or self.net_ip == '':
            return None
        return self.net_ip

    def get_net_pfx_len(self):
        if self.net_pfx_len is None or self.net_pfx_len == 0:
            return self.platform.get_net_pfx_len()
        return self.net_pfx_len

    def get_net_gw(self):
        if self.net_gw is None or self.net_gw == '':
            return self.platform.get_net_gw()
        return self.net_gw

    def get_admin_user(self):
        if self.admin_user is None or self.admin_user == '':
            return self.platform.get_admin_user()
        return self.admin_user

    def get_admin_keys(self):
        return self.platform.get_admin_keys()

    def get_system_packages(self):
        platform_packages = self.platform.get_system_packages() or []
        machine_packages = self.pkg_install.all() or []
        packs = list(set().union(platform_packages, machine_packages))
        packages = []
        for pack in packs:
            packages.append(pack.name)
        return packages

    def get_system_packages_pip(self):
        platform_packages = self.platform.get_system_packages_pip() or []
        machine_packages = self.pkg_install_pip.all() or []
        packs = list(set().union(platform_packages, machine_packages))
        packages = []
        for pack in packs:
            packages.append(pack.name)
        return packages

    # ----
    # generate host vars

    def host_vars(self):
        variables = {}

        # vmware
        vmware_vars = {}
        vmware_vars['name'] = self.get_vmware_name()
        vmware_vars['template'] = self.get_vmware_template()
        vmware_vars['guest_id'] = self.get_vmware_guest_id()
        vmware_vars['folder'] = self.get_vmware_folder()
        vmware_vars['datastore'] = self.get_vmware_datastore()

        hw_vars = {}
        hw_vars['cpus'] = self.get_hw_cpus()
        hw_vars['ram'] = self.get_hw_ram()
        hw_vars['root_size'] = self.get_hw_root_size()
        hw_vars['root_size_percent_vg'] = self.get_hw_root_size_as_percent_vg()

        net_vars = {}
        net_vars['vlan'] = self.get_net_vlan()
        net_vars['hostname'] = self.get_net_hostname()
        net_vars['local_aliases'] = self.get_net_names()
        net_vars['search_domains'] = self.get_net_search_domains()
        net_vars['ip'] = self.get_net_ip()
        net_vars['pfx_len'] = self.get_net_pfx_len()
        net_vars['gw'] = self.get_net_gw()

        admin_vars = {}
        admin_user = self.get_admin_user()
        if admin_user:
            admin_vars['user'] = admin_user
            admin_ssh_keys = self.get_admin_keys()
            if admin_ssh_keys:
                admin_vars['ssh_keys'] = "\n".join(admin_ssh_keys)
        else:
            admin_vars = None

        packages_vars = {}
        system_packages = self.get_system_packages()
        if system_packages:
            packages_vars['system'] = system_packages
        pip_packages = self.get_system_packages_pip()
        if pip_packages:
            packages_vars['pip'] = pip_packages

        variables['vmware'] = vmware_vars
        variables['hw'] = hw_vars
        variables['net'] = net_vars
        if admin_vars:
            variables['admin'] = admin_vars
        if packages_vars:
            variables['packages'] = packages_vars

        # si_osug.zabbix.group

        return variables

    def needs_updated(self):
        return self.last_applied is None or self.last_applied < self.last_update
