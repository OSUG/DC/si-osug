# -*- encoding: utf8 -*-

from django.db import models


class Template(models.Model):
    """
    templates used in vmware
    """
    name = models.CharField(max_length=64, null=False, blank=False, unique=True)
    display_name = models.CharField(max_length=256, null=True, blank=True, unique=False)
    guest_id = models.ForeignKey('VMGuestOs', on_delete=models.DO_NOTHING)

    def __str__(self):
        _gid = ""
        # print(dir(self))
        try:
            _gid = self.guest_id
        except:
            print("template name : ", self.name)
        return '%s (%s)' % (self.name, _gid)


class VMGuestOs(models.Model):
    """
    Guest OS types
    """
    name = models.CharField(max_length=64, null=False,
                            blank=False, unique=True)
    description = models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        return self.name


class VMDatastore(models.Model):
    """
    Datastore locations
    """
    name = models.CharField(max_length=64, null=False,
                            blank=False, unique=True)

    def __str__(self):
        return self.name


class VMDisk(models.Model):
    """
    disks used by vms
    """
    datastore = models.ForeignKey(VMDatastore, on_delete=models.DO_NOTHING, related_name='disks')
    vm = models.ForeignKey('Machine', on_delete=models.DO_NOTHING, related_name='disks')
    uuid = models.TextField(max_length=36, null=False, blank=False, default='00000000-0000-0000-0000-000000000000')
    disk_id = models.TextField(null=True, blank=True)
    index = models.IntegerField(null=False, blank=False)
    size_b = models.BigIntegerField(null=False, blank=False)
    label = models.TextField(null=False, blank=False)

    def size_gb(self):
        return self.size_b/(1024*1024*1024)

class Vlan(models.Model):
    """
    Vlans and virtual switches
    """
    name = models.CharField(max_length=256, null=False, blank=False, unique=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
