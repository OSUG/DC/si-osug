# -*- encoding: utf8 -*-

from django.db import models


class Config(models.Model):
    """
    packages installable via package manager
    """
    key = models.CharField(max_length=64, null=False,
                           blank=False, unique=True)
    value = models.CharField(max_length=256, null=False,
                             blank=False)

    def __str__(self):
        return self.key
