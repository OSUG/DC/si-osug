# -*- encoding: utf8 -*-

import datetime
import inspect
import decimal

from django.contrib.admin.models import LogEntry
from django.contrib.postgres.fields import JSONField
from django.contrib.sessions.models import Session
from django.db import models
from django.db.models.signals import (m2m_changed, post_save, pre_save)
from django.dispatch import receiver
from ..middleware import (get_current_authenticated_user)
from django.conf import settings

class History(models.Model):
    OP_ADD = 1
    OP_MODIFY = 2
    OP_DELETE = 3
    OPERATION_CHOICES = (
        (OP_ADD, 'ADD',),
        (OP_MODIFY, 'MODIFY',),
        (OP_DELETE, 'DELETE',),
    )

    # main info, used in admin list page
    timestamp = models.DateTimeField(unique=True, auto_now_add=True)
    username = models.CharField(max_length=32, null=True, blank=True)
    model = models.CharField(max_length=80, null=False, blank=False)
    operation = models.IntegerField(null=False, blank=False,
                                    choices=OPERATION_CHOICES, default=OP_ADD)
    # values (only displayed on the details page)
    primary_key = JSONField(null=False, blank=False, default=dict)
    old_values = JSONField(null=True, blank=True)
    new_values = JSONField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Histories'


# -------------------------------
# this is called pre_save.
# NOTE: does NOT handle the many_to_many fields

def format_value(value):
    if isinstance(value, datetime.datetime):
        return value.isoformat()
    if isinstance(value, decimal.Decimal):
        return str(value)
    return value


def can_ignore(instance):
    return isinstance(instance,
                      (History,
                       Session,
                       LogEntry, ))

def get_primary_key(instance):
    # pk = []
    opts = instance._meta
    fields = opts.get_fields(include_parents=True, include_hidden=True)
    primary_key = {}
    for f in fields:
        try:
            is_pk = getattr(f, 'primary_key')
        except AttributeError:
            pass
        else:
            if is_pk:
                field = getattr(instance, f.name)
                primary_key[f.name] = format_value(field)
    return primary_key


@receiver(pre_save)
def log_changes_prepare(instance, **kwargs):
    if can_ignore(instance):
        return

    debug = getattr(settings, "DEBUG_HISTORY", False)

    if debug:
        print("PRE_SAVE", " | ", instance, " | ", kwargs)

    opts = instance._meta
    fields = opts.get_fields(include_parents=True, include_hidden=True)

    model_name = instance.__class__.__name__

    operation = History.OP_MODIFY
    try:
        old_object = instance.__class__.objects.get(pk=instance.pk)
    except instance.__class__.DoesNotExist:
        operation = History.OP_ADD

    primary_key = get_primary_key(instance)
    old_values = {}
    new_values = {}

    for f in fields:
        try:
            if operation == History.OP_MODIFY:
                ov = getattr(old_object, f.name)
            nv = getattr(instance, f.name)
        except ValueError:
            pass
        except AttributeError:
            pass
        else:
            if f.is_relation:
                # relation field type
                if f.many_to_one:
                    if operation == History.OP_MODIFY:
                        ov_value = None
                        if ov is not None and "pk" in dir(ov): 
                            ov_value = ov.pk
                        old_values[f.name] = format_value(ov_value)
                    if operation == History.OP_ADD or ov != nv:
                        nv_value = None
                        if nv is not None and "pk" in dir(nv):
                            nv_value = nv.pk
                        new_values[f.name] = format_value(nv_value)
                if f.many_to_many:
                    continue
                # there are other types I believe but not used yet
            else:
                if operation == History.OP_MODIFY:
                    old_values[f.name] = format_value(ov)
                if operation == History.OP_ADD or ov != nv:
                    new_values[f.name] = format_value(nv)

    if operation == History.OP_ADD:
        old_values = None

    h = History()
    try:
        h.username = get_current_authenticated_user().username
    except AttributeError:
        h.username = None

    if debug:
        print(">>>> PRE_SAVE %s"%(h.username))
    h.model = model_name
    h.operation = operation
    h.primary_key = primary_key
    h.old_values = old_values
    h.new_values = new_values
    h.save()


@receiver(m2m_changed)
def log_changes_m2m(instance, **kwargs):
    if can_ignore(instance):
        return

    debug = getattr(settings, "DEBUG_HISTORY", False)

    if debug:
        print('M2M_CHANGED', " | ", instance, " | ", kwargs)

    # pk_set = kwargs.pop('pk_set', None)
    action = kwargs.pop('action', None)
    sender = kwargs.pop('sender', None)
    # model = kwargs.pop('model', None)
    if debug:
        print(instance.__class__.__name__)
    # format is "<instance class name>_<field name>"
    field_name = sender._meta.object_name[len(instance.__class__.__name__)+1:]

    
    try:
        username = get_current_authenticated_user().username
    except AttributeError:
        username = None
    if debug:
        print(username)

    field = getattr(instance, field_name)
    values = []
    for r in field.all():
        values.append(r.pk)

    primary_key = get_primary_key(instance)

    history = History.objects.filter(username=username,
                                     model=instance.__class__.__name__,
                                     primary_key=primary_key
                                     ).latest('timestamp')

    if action in ('pre_add', 'pre_remove', ):
        old_values = history.old_values
        if old_values:
            old_values[field_name] = values
        history.old_values = old_values
        history.save()
    if action in ('post_add', 'post_remove', ):
        new_values = history.new_values
        new_values[field_name] = values
        history.new_values = new_values
        history.save()


def dump_object(ob):
    for f in dir(ob):
        try:
            attr = getattr(ob, f)
        except AttributeError:
            print(f, " << INACCESSIBLE")
        else:
            if inspect.isclass(attr):
                print(f, ' : ', attr.__class__.__name__)
            else:
                print(f, ' : ', str(attr))


@receiver(post_save)
def log_changes_commit(instance, **kwargs):
    debug = getattr(settings, "DEBUG_HISTORY", False)

    if debug:
        print('-----------------------------------------')
        print('POST_SAVE', " | ", instance, " | ", kwargs)
        if isinstance(instance, History):
            print("instance of History, skipping")
            return

        print(instance,  kwargs)
