# -*- encoding: utf8 -*-

from django.db import models

from .team import Team
from .user import User
from .vmware import Template, VMDatastore, Vlan

class Platform(models.Model):
    """
    a platform for a particular team
      * has a parent team
    """
    name = models.CharField(max_length=32, null=False, blank=False)
    team = models.ForeignKey(Team, on_delete=models.DO_NOTHING,
                             related_name='platforms', null=True)
    users = models.ManyToManyField(User, related_name='platforms',
                                   blank=True)
    # machine defaults for the platform

    # VMware

    vm_template = models.ForeignKey(Template, on_delete=models.DO_NOTHING,
                                    null=True, blank=True)
    vm_folder = models.CharField(max_length=128, null=True, blank=True)
    vm_datastore = models.ForeignKey(VMDatastore,
                                     on_delete=models.DO_NOTHING, null=True,
                                     blank=True)

    # hardware virtuel

    hw_cpus = models.IntegerField(null=True, blank=True)
    hw_memory = models.IntegerField(null=True, blank=True)  # Gigabytes
    hw_root = models.IntegerField(null=True, blank=True)  # Gigabytes

    # network configuration

    net_vlan = models.ForeignKey(Vlan, on_delete=models.DO_NOTHING, null=True, blank=True)
    # net_vlan_text = models.CharField(max_length=128, null=True, blank=True)
    net_search_domains = models.CharField(max_length=256, null=True,
                                          blank=True)
    net_pfx_len = models.IntegerField(null=True, blank=True)
    net_gw = models.GenericIPAddressField(null=True, blank=True)

    # user for local administration

    admin_user = models.CharField(max_length=32, null=True, blank=True)

    class Meta:
        unique_together = (('name', 'team', ), )

    def __str__(self):
        return '%s - %s' % (self.team.name, self.name)

    # ----
    # Accessor functions

    def get_vmware_template(self):
        if self.vm_template is None:
            return self.team.get_vmware_template()
        return self.vm_template.name

    def get_vmware_guest_id(self):
        if self.vm_template is None:
            return self.team.get_vmware_guest_id()
        return self.vm_template.guest_id.name

    def get_vmware_folder(self):
        if self.vm_folder is None or self.vm_folder == '':
            return self.team.get_vmware_folder()
        return self.vm_folder

    def get_vmware_datastore(self):
        if self.vm_datastore is None:
            return self.team.get_vmware_datastore()
        return self.vm_datastore.name

    def get_hw_cpus(self):
        if self.hw_cpus is None or self.hw_cpus == 0:
            return self.team.get_hw_cpus()
        return self.hw_cpus

    def get_hw_ram(self):
        if self.hw_memory is None or self.hw_memory == 0:
            return self.team.get_hw_ram()
        return self.hw_memory

    def get_hw_root_size(self):
        if self.hw_root is None or self.hw_root == 0:
            return self.team.get_hw_root_size()
        return self.hw_root

    def get_net_vlan(self):
        if self.net_vlan is None or self.net_vlan == '':
            return self.team.get_net_vlan()
        return self.net_vlan.name

    def get_net_search_domains(self):
        if self.net_search_domains is None or self.net_search_domains == '':
            return self.team.get_net_search_domains()
        return self.net_search_domains

    def get_net_pfx_len(self):
        if self.net_pfx_len is None or self.net_pfx_len == 0:
            return self.team.get_net_pfx_len()
        return self.net_pfx_len

    def get_net_gw(self):
        if self.net_gw is None or self.net_gw == '':
            return self.team.get_net_gw()
        return self.net_gw

    def get_admin_user(self):
        if self.admin_user is None or self.admin_user == '':
            return self.team.get_admin_user()
        return self.admin_user

    def get_admin_keys(self):
        if self.users is None or self.users.count() == 0:
            return self.team.get_admin_keys()
        keys = []
        for user in self.users.all():
            keys += user.get_ssh_keys()
        return keys

    def get_system_packages(self):
        return self.team.get_system_packages()

    def get_system_packages_pip(self):
        return self.team.get_system_packages_pip()

    # ----
    # other platform functions
