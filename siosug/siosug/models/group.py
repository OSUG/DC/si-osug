# -*- encoding: utf8 -*-

import logging

from passlib.hash import ldap_pbkdf2_sha512
from django.db import models

class UserGroup(models.Model):
    """
    user groups, used to generate LDAP groups
    """
    unix_gid = models.IntegerField(primary_key=True, unique=True, null=False, blank=False)
    groupname = models.CharField(unique=True, max_length=32, null=False,
                                blank=False)
    users = models.ManyToManyField('User', related_name='groups')
