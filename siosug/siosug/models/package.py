# -*- encoding: utf8 -*-

from django.db import models


class Package(models.Model):
    """
    packages installable via package manager
    """
    name = models.CharField(max_length=64, null=False,
                            blank=False, unique=True)

    def __str__(self):
        return self.name


class PythonPackage(models.Model):
    """
    packages installable via pip
    """
    name = models.CharField(max_length=64, null=False,
                            blank=False, unique=True)

    def __str__(self):
        return self.name
