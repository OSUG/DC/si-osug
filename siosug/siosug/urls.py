"""siosug URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.views.generic.base import RedirectView

from .views import (Dashboard, Inventory, Login, Logout, Platforms, Vm,
                    VmVariable, VmList, VMsPerTeam)

admin.autodiscover()

FAVICON_VIEW = RedirectView.as_view(url='/static/images/favicon.ico', permanent=True)

urlpatterns = [
    re_path(r'^favicon\.ico$', FAVICON_VIEW),
    path('admin/', admin.site.urls),
    path('API/inventory', Inventory.as_view(), name='api-inventory'),
    path('', Dashboard.as_view(), name='index'),
    path('login', Login.as_view(), name='login-page'),
    path('logout', Logout.as_view(), name='logout'),
    path('my/platforms', Platforms.as_view(), name='my-platforms'),
    path('my/vm-list', VmList.as_view(), name='my-vm-list'),
    path('my/stats/vms-per-team', VMsPerTeam.as_view(), name='vms-per-team'),
    path('manage/vm/new/', Vm.as_view(), name='vm-new'),
    path('manage/vm/<int:vm_id>/', Vm.as_view(), name='vm-detail'),
    path('manage/vm/<slug:vm_id>/<slug:variable>/', VmVariable.as_view(), name='vm-variable'),
]
