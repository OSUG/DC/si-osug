"""
personalized login/logout page
"""

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.views.generic import View

import logging

class Login(View):
    """
    Login page and procedure
    """
    def get(self, request, *args, **kwargs):    # pylint: disable=W0613
        """
        generates the login page
        """
        return render(request, 'login-page.j2.html')

    def post(self, request, *args, **kwargs):    # pylint: disable=W0613
        """
        does the login procedure and redirects to the page the user attempted
        to connect to
        """
        redirect_to = request.GET.get('next', None)
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        if username is None or password is None:
            return render(request, 'login-page.j2.html')
        try:
            user = authenticate(username=username, password=password)
        except AttributeError:
            logger = logging.getLogger("django.request")
            logger.critical("failed authenticating user")
            user = None
        if user is None:
            return render(request, 'login-page.j2.html')
        login(request, user)
        if redirect_to is not None:
            return redirect(redirect_to)


class Logout(View):
    """
    logout procedure
    """
    def get(self, request, *args, **kwargs):    # pylint: disable=W0613
        """
        logs the user out of the system and redirects to the index page
        """
        logout(request)
        return redirect('index')
    
