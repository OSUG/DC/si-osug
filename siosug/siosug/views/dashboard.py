"""
User's dashboard
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import View


class Dashboard(View):
    """
    User's dashboard
    """
    @method_decorator(login_required(login_url='login-page'))
    def get(self, request, *args, **kwargs):    # pylint: disable=W0613
        """
        Display the user's dashboard (currently empty)
        """
        return render(request, 'dashboard.j2.html')
