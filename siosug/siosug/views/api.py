from django.http import JsonResponse
from django.views.generic import View
from ..models import Team, Platform


class Inventory(View):
    def get(self, request, *args, **kwargs):
        # this will generate an inventory for all machines with variables
        # and everything
        response = {}
        all_hosts = []
        all_groups = []
        host_vars = {}  
        for p in Platform.objects.all():  # pylint: disable=E1101 
            group_name = '%s-%s' % (p.team.name, p.name)
            all_groups.append(group_name)
            host_list = []
            for h in p.machines.all():
                # add host to inventory
                if not h.net_hostname:
                    print("error, net_hostname empty for machine '%s'"%(h.vm_name))
                    continue
                host_name = h.net_hostname      
                try:
                    machine_vars = {'si_osug': h.host_vars()}
                except:
                    # skip the machine if any error happens
                    continue
                host_list.append(host_name)
                all_hosts.append(host_name)
                # get variables for host
                host_vars[host_name] = machine_vars

            group_data = {}
            group_data['hosts'] = host_list
            response[group_name] = group_data
        
        for t in Team.objects.all():  # pylint: disable=E1101
            team_name = t.name
            all_groups.append(team_name)
            team_groups = []
            for p in t.platforms.all():
                team_groups.append("%s-%s"%(team_name,p.name))
            response[team_name] = {
                'children': team_groups
            }

        all_groups.append('ungrouped')
        response['all'] = {
            'hosts': all_hosts,
            'children': all_groups
        }
        response['ungrouped'] = []
        response['_meta'] = {'hostvars': host_vars}
        return JsonResponse(response)
