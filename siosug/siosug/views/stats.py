"""
Module d'edition / création d'un enregistrement de VM
"""

from django.contrib.auth.decorators import login_required
# from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import View

from ..models import Machine, Platform, Team, Template, Vlan, VMDatastore

class VMsPerTeam(View):
    """
    List the VMs per group
    """
    @method_decorator(login_required(login_url='login-page'))
    def get(self, request, *args, **kwargs):    # pylint: disable=W0613

        teams = Team.objects.all()
        team_data = {}
        for t in teams:
            nb_vm_team = 0
            platforms = t.platforms
            for p in platforms.all():
                machines = p.machines.all()
                nb_machines = machines.count()
                nb_vm_team += nb_machines
            team_data[t.name] = nb_vm_team
        data = { 'team_data': team_data }
        return render(request, 'vms-per-team.j2.html', context=data)
