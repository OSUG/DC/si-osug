"""
User's platforms page
"""

import logging

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import View

from ..models import Platform


class Platforms(View):
    """
    User platforms page
    """
    @method_decorator(login_required(login_url='login-page'))
    def get(self, request, *args, **kwargs):    # pylint: disable=W0613
        """
        Displays the user platforms page
        """
        user = request.user
        logger = logging.getLogger("OSUG.User")
        logger.error("test2")
        logger.error("User: %s", str(user))
        platforms = Platform.objects.all()
        q_team = Q(team__users__pk=user.pk)
        q_users = Q(users__pk=user.pk)
        platforms = Platform.objects.filter(q_team|q_users)
        data = {
            "platforms": platforms
        }
        print(data)
        return render(request, 'platforms.j2.html', context=data)
