"""
User's VMs page module
"""

from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import View

from ..models import Machine


class VmList(View):
    """
    User's VMs page
    """
    @method_decorator(login_required(login_url='login-page'))
    def get(self, request, *args, **kwargs):    # pylint: disable=W0613
        """
        Displays the vms the user is allowed to manage
        """
        user = request.user
        machines = Machine.objects.all().order_by('platform__team__name', 'platform__name')
        if not user.is_admin:
            q_team = Q(platform__team__users__pk=user.pk)
            q_platform = Q(platform__users__pk=user.pk)
            machines = machines.filter(q_team|q_platform)
        # print(machines)
        data = {
            "machines": machines
        }
        # print(data)
        return render(request, 'vm-list.j2.html', context=data)
