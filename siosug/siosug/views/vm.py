"""
Module d'edition / création d'un enregistrement de VM
"""
import json
from datetime import datetime

from django.contrib.auth.decorators import login_required
# from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import View

from ..models import Machine, Platform, Team, Template, Vlan, VMDatastore, ActionRecord

SRC_TEAM = "TEAM"
SRC_PLATFORM = "PLATFORM"
SRC_MACHINE = "MACHINE"

# --------------------------------------------------------------------------------
#
# Vm class
#
# handling of the VM form
#
# --------------------------------------------------------------------------------

class Vm(View):
    """
    VM view

    has handlers for get and post
    """
    save_vm = False
    errors = {}

    # --------------------------------------------------------------------------------
    #
    # Utility methods
    #

    def inherited_var(self, vm_object, var_value, obj_var):
        """
        handles generic inherited var

        gets the value for a particular variable,
        check if the value is identical to the value we can inherit.
        in that case, don't store anything in the vm_object
        else, store the var_value in the vm_object, as it's different from the
        value that should be inherited
        """
        ## handle inheritance
        # check if the value set by the user is identical to one of the potentially inherited values
        print("checking inheritance for '%s'"%(obj_var))
        pf_object = vm_object.platform
        pf_var = getattr(pf_object, obj_var, None)
        if pf_var:
            print("pf_var", pf_var, var_value)
            if var_value:
                if pf_var == var_value:
                    # don't fill the value locally
                    print('var_value is identical to the platform one')
                    return True
                else:
                    print("var value is different from pf_var")
                    return False
            print("var_value is empty, inherit from platform")
            return True
        else:
            # if pf_var is None, look in team
            tm_object = pf_object.team
            tm_var = getattr(tm_object, obj_var, None)
            if tm_var:
                print("tm_var '%s' | var_value '%s'"%(str(tm_var), str(var_value)))
                if var_value:
                    if tm_var == var_value:
                        # don't fill the value locally
                        print("var_value is identical to the team one")
                        return True
                    else:
                        print("var_value is different from tm_var")
                        return False
                print("var_value is empty, inherit from the team")
                return True
        # neither pf_var nor tm_var were good, set the value locally
        print("no inheritance value set")
        # setattr(vm_object, obj_var, var_value)
        # print("value for vm_object."+obj_var, var_value)
        # self.save_vm = True
        return False

    def inherited_integer(self, data, vm_object, form_var, obj_var, empty_msg, errors):
        """
        handles inherited integer variables
        """
        print("---------- INTEGER")
        var_value = data[form_var]
        if var_value:
            if not isinstance(var_value, int):
                var_value = var_value.strip()
                if var_value.isdecimal():
                    var_value = int(var_value)
            print("var_value '%d'"%(var_value))

            inherits = self.inherited_var(vm_object, var_value, obj_var)
            if inherits:
                return True
            else:
                print("no value was found in the inheritance tree for ", obj_var)
                print("var value '%d'"%(var_value))
                setattr(vm_object, obj_var, var_value)
                print("value for vm_object."+obj_var, var_value)
                self.save_vm = True
                return True
        errors[form_var] = empty_msg
        return False

    def inherited_string(self, data, vm_object, form_var, obj_var, empty_msg, errors):
        """
        handles inherited string variables
        """
        print("---------- STRING")
        var_value = data[form_var]
        print("original string: '%s'"%(str(var_value)))
        if var_value:
            var_value = var_value.strip()
        # find if the value can be used
        print("var_value '%s'"%(var_value))
        inherits = self.inherited_var(vm_object, var_value, obj_var)
        if inherits:
            return True
        else:
            print("no value was found in the inheritance tree for ", obj_var)
            print("var value '%s'"%(var_value))
            if not var_value:
                print("setting error message ", empty_msg)
                errors[form_var] = empty_msg
                return False
        setattr(vm_object, obj_var, var_value)
        print("value for vm_object."+obj_var, var_value)
        self.save_vm = True
        return True

    def inherited_choice(self, data, vm_object, form_var, obj_var, empty_msg, index_msg, errors):
        """
        handles inherited choice variables (from select field -> foreign key)
        note: mostly similar to integer
        """
        print("---------- CHOICE")
        var_value = data[form_var]
        print("init var_value '%s'"%(var_value))
        if var_value:
            if not isinstance(var_value, int):
                var_value = var_value.strip()
                print("cleanup var_value '%s'"%(var_value))
                if var_value and var_value.isdecimal():
                    var_value = int(var_value)
            print("int(var_value) %d"%(var_value))
            # go find the value...
            value_object_class = vm_object._meta.get_field(obj_var).remote_field.model
            try:
                value_instance = value_object_class.objects.get(pk=var_value)
            except value_object_class.DoesNotExist:
                errors[form_var] = index_msg % var_value
                return False
            print("value_instance", value_instance)
            inherits = self.inherited_var(vm_object, value_instance, obj_var)
            print("is var_value inherited ? ", inherits)
            if not inherits:
                print ("is not inherited, save the ", value_instance)
                setattr(vm_object, obj_var, value_instance)
                self.save_vm = True
                return True
            print ("value is inherited, skip")
            return True
        print ("setting error message '"+empty_msg+"'")
        errors[form_var] = empty_msg
        return False

    # --------------------------------------------------------------------------------
    #
    # GET handler
    #

    @method_decorator(login_required(login_url='login-page'))
    def get(self, request, vm_id=None):    # pylint: disable=W0613
        """
        GET Handler:

        displays the vm form
        """
        data = dict()
        if vm_id is not None:
            # get data for the vm
            try:
                machine = Machine.objects.get(pk=vm_id)
            except Machine.DoesNotExist:
                return HttpResponse(status=404)
            data['machine'] = machine
        return render(request, 'vm.j2.html', context=data)

    # --------------------------------------------------------------------------------
    #
    # POST handler
    #

    @method_decorator(login_required(login_url='login_page'))
    def post(self, request, vm_id=None):
        """
        POST Handler:

        * gets the vm data back
        * attempts to update / add a vm into the database
        """
        print("----------------------------------------------------------------------------------")
        print(request.content_type)
        if request.content_type != "application/json":
            return HttpResponse(status=415)
        try:
            data = json.loads(request.body)
        except json.decoder.JSONDecodeError:
            print("Json decode error")
            print(request.body)
            return HttpResponse(status=400)
        virtual_machine = None
        if vm_id is None:
            # generate a new vm
            virtual_machine = Machine()
            virtual_machine.vm_name = None
            virtual_machine.net_hostname = None
            action = ActionRecord.AR_CREATE_VM
        else:
            virtual_machine = Machine.objects.get(id=vm_id)
            action = ActionRecord.AR_UPDATE_VM
            
        print(data)

        #
        # initialize a new empty list of errors !!
        #
        errors = dict()

        #
        # vm_name
        #
        vm_name = data['vm_name']
        print("vm_name '%s'"% (vm_name))
        if vm_name:
            vm_name = vm_name.strip()
        if not vm_name:
            errors['vm_name'] = "Le nom de la VM ne peut etre vide"
            print("ERROR: %s"%(errors["vm_name"]))
            
        else:
            # check if we have a vm with the same name already
            try:
                vm_with_same_name = Machine.objects.get(vm_name=vm_name)
            except Machine.DoesNotExist:
                pass
            else:
                if not vm_id:
                    # exit immediately with an error IF we are creating a new vm
                    msg = 'Machine with name %s already exists' % vm_name
                    errors['vm_name'] = msg
                    print(msg)
                    data = {}
                    data['ok'] = False
                    data['already_exists'] = vm_with_same_name.pk
                    data['errors'] = errors
                    return JsonResponse(data)

            if virtual_machine.vm_name is not None:
                # record a vm name change
                pass

            virtual_machine.vm_name = vm_name
            self.save_vm = True

        # plateform (should not be None)
        platform_id = data['platform']
        if platform_id:
            platform_id = platform_id.strip()
        if not platform_id:
            errmsg = "La plateforme doit être renseignée"
            errors['platform'] = errmsg
            print("ERROR : "+errmsg)
        elif platform_id.isdecimal():
            # set platform variable
            try:
                platform = Platform.objects.get(pk=platform_id)
            except Platform.DoesNotExist:
                errors['platform'] = "La plateforme avec l'index "+\
                    str(platform_id)+" n'existe pas"
            else:
                virtual_machine.platform = platform
                self.save_vm = True
        else:
            errmsg = "La plateforme doit etre un nombre entier"
            errors['platform'] = errmsg
            print("ERROR : "+errmsg)
        
        # if platform is ok, otherwise, don't bother with the rest...
        if virtual_machine.platform:
            #
            # dns aliases
            #
            dns_aliases = data['dns_aliases']
            if not dns_aliases:
                errors['dns_aliases'] = "La liste des alias DNS ne peut etre vide"
            else:
                # check aliases validity
                # generate the list
                main_alias = None
                aliases = []
                for alias in dns_aliases:
                    alias_name = alias['alias']
                    if alias['main']:
                        main_alias = alias_name
                    aliases.append(alias_name)
                if main_alias:
                    print("main_alias : '"+main_alias+"'")
                else:
                    # we MUST have some aliases here
                    if aliases:
                        # user has not specified a main alias,
                        # take the first one...
                        main_alias = aliases[0]
                    else:
                        print("ERROR: no main alias, SHOULD NOT HAPPEN")
                        errors['dns_aliases'] = "Aucun alias a selectionner dans une liste vide"
                print("aliases : "+str(aliases))
                virtual_machine.net_hostname = main_alias
                virtual_machine.net_names = " ".join(aliases)
                self.save_vm = True

            # Nom d'utilisateur d'administration
            self.inherited_string(data, virtual_machine, 'admin_login', 'admin_user',
                                  "Le login administrateur ne peut etre vide", errors)
            # dossier dans lequel on range la VM
            self.inherited_string(data, virtual_machine, 'folder', 'vm_folder',
                                  "Le dossier ne peut être vide", errors)
            # template avec laquelle on crée la VM
            self.inherited_choice(data, virtual_machine, 'template', 'vm_template',
                                  "La template doit être renseignée",
                                  "La template avec l'index %d n'existe pas", errors)
            # nombre de CPUs (entier)
            self.inherited_integer(data, virtual_machine, 'cpus', 'hw_cpus',
                                   "Le nombre de CPU doit être un entier", errors)
            # mémoire virtuelle (en Mo)
            self.inherited_integer(data, virtual_machine, 'memory', 'hw_memory',
                                   "La quantité de mémoire doit être un entier", errors)
            # espace de stockage initial (en Go)
            self.inherited_integer(data, virtual_machine, 'root_size', 'hw_root',
                                   "La taille du disque principal doit etre un entier", errors)
            # datastore ou est situé l'espace de stockage initial
            self.inherited_choice(data, virtual_machine, 'root_datastore', 'vm_datastore',
                                  "L'emplacement du disque principal doit être renseigné",
                                  "Le Datastore avec l'index %d n'existe pas", errors)
            # vlan dans lequel se présente l'interface réseau de la VM
            self.inherited_choice(data, virtual_machine, 'vlan', 'net_vlan',
                                  "Le VLAN doit être renseigné",
                                  "Le VLAN avec l'index %d n'existe pas", errors)

            #
            # ip address for the vm
            #
            net_ip = data['net_ip']
            if net_ip is not None:
                net_ip = net_ip.strip()
            if net_ip is None or not net_ip:
                errors['net_ip'] = "L'adresse IP ne peut être vide"
            else:
                virtual_machine.net_ip = net_ip
                self.save_vm = True

            #
            # gateway this VM is to use to access the internet
            #
            net_gw = data['net_gw']
            if net_gw is None:
                self.errors['net_gw'] = "L'adresse de la passerelle doit être renseignée"
                gw_ip = None
                gw_pfx_len = None
            else:
                gw_ok = True
                gw_ip = net_gw['ip_addr']
                if gw_ip is not None:
                    gw_ip = gw_ip.strip()
                if gw_ip is None or not gw_ip:
                    errors['net_gw'] = "L'adresse de la passerelle ne peut être vide"
                    gw_ok = False
                gw_pfx_len = net_gw['pfx_len']
                if gw_pfx_len and not isinstance(gw_pfx_len, int):
                    gw_pfx_len = gw_pfx_len.strip()
                if gw_ip is not None and (gw_pfx_len is None or not gw_pfx_len):
                    errors['net_gw'] = \
                        "La longueur du prefixe de l'adresse de la passerelle ne peut être vide"
                    gw_ok = False
                # TODO: do more tests
                if gw_ok:
                    virtual_machine.net_gw = gw_ip
                    virtual_machine.net_pfx_len = gw_pfx_len
                    self.save_vm = True

            #
            # default dns search domains
            #
            # Note: net_search_domains n'est pas nécessaire
            net_search_domains = data['net_search_domains']
            if net_search_domains is not None:
                net_search_domains = net_search_domains.strip()
                if net_search_domains != virtual_machine.net_search_domains:
                    virtual_machine.net_search_domains = net_search_domains
                    self.save_vm = True

            #
            # save vm data
            #
            if errors:
                print('errors have been detected, unable to save')
                for field in errors:
                    print(field, errors[field])
            else:
                print("Should save vm record: "+str(self.save_vm))
                if self.save_vm:
                    # set last_update
                    virtual_machine.last_update = datetime.now()
                    virtual_machine.save()

                    # create the action record
                    action_record = ActionRecord()
                    action_record.verb = action
                    action_record.params = { 'vm_id': virtual_machine.pk }
                    action_record.save()
        
        orig_data = data
        data = {}
        if errors:
            data["ok"] = False
            data["errors"] = errors
            data['values'] = orig_data
        else:
            data["ok"] = True
            data['redirect_to'] = reverse('vm-detail', kwargs={'vm_id': virtual_machine.id})
        return JsonResponse(data)

# --------------------------------------------------------------------------------
#
# VmVariable class
#
# handles the ajax requests for VM variables
#
# --------------------------------------------------------------------------------

class VmVariable(View):
    """
    Class VmVariable

    Views to provide data to the VM form through ajax
    """

    _vm = None

    @method_decorator(login_required(login_url='login-page'))
    def get(self, request, vm_id=None, variable=None):
        """
        GET Handler

        returns info about the requested variable as json data.
        returns error 412 if the variable does not exist
        """
        data = dict()
        if vm_id is not None and variable is not None:
            data = dict()

            # TODO: check if logged user can access the vm data

            if vm_id.isnumeric():
                try:
                    self._vm = Machine.objects.get(pk=vm_id)
                except Machine.DoesNotExist:
                    print("machine ", vm_id, "does not exist")
                    return HttpResponse(status=404)
            else:
                self._vm = None

            data['var'] = getattr(self, "var_"+variable, None)

            data['options'] = getattr(self, "options_"+variable, None)

            try:
                return JsonResponse(data)
            except TypeError:
                print(type(data), data, "is not a dict")
                return HttpResponse(status=500)

        # error 412 precondition failed
        return HttpResponse(status=412)

    def get_inherited_variable(self, var_name):
        """
        returns the value and source of the variable, following the
        inheritance path
        Machine -> Platform -> Team
        """
        _source = SRC_MACHINE
        _var = None
        _var = getattr(self._vm, var_name, None)
        if _var is not None:
            return (_source, _var,)
        _platform = self._vm.platform
        if _platform is None:
            return None
        _source = SRC_PLATFORM
        _var = getattr(_platform, var_name, None)
        if _var is not None:
            return (_source, _var,)
        _team = _platform.team
        if _team is None:
            return None
        _source = SRC_TEAM
        _var = getattr(_team, var_name, None)
        if _var is not None:
            return (_source, _var,)
        return None

    #
    # variables getter/setter
    #

    @property
    def var_vm_name(self):
        """
        returns the data for the vm_name variable
        """
        vm_name_dict = dict()
        vm_name_dict['value'] = self._vm.vm_name
        return vm_name_dict

    @property
    def var_dns_aliases(self):
        """
        returns the data for the dns_aliases variable
        """
        dns_aliases_dict = dict()
        dns_aliases_list = list()
        names = self._vm.net_names.split(" ")
        name = self._vm.net_hostname
        if name is not None and name:
            name.strip()
            if name in names:
                names.remove(name)
            dns_alias = dict()
            dns_alias["main"] = True
            dns_alias["alias"] = name
            dns_aliases_list.append(dns_alias)
        for name in names:
            name = name.strip()
            if name:
                dns_alias = dict()
                dns_alias["main"] = False
                dns_alias["alias"] = name
                dns_aliases_list.append(dns_alias)
        dns_aliases_dict["value"] = dns_aliases_list
        return dns_aliases_dict

    @property
    def var_admin_login(self):
        """
        returns the data for the admin_login variable
        """
        # if self._vm is not None:
        #     user = self._vm.admin_user
        # else:
        #     user = None
        src, user = self.get_inherited_variable("admin_user")
        user_val = dict()
        user_val["source"] = src
        user_val["value"] = user    
        return user_val

    @property
    def options_admin_login(self):
        """
        lists all existing admin users
        """
        ulist = list()
        for machine in Machine.objects.all():
            user = machine.admin_user
            if user is not None and user not in ulist:
                ulist.append(user)
        for platform in Platform.objects.all():
            user = platform.admin_user
            if user is not None and user not in ulist:
                ulist.append(user)
        for team in Team.objects.all():
            user = team.admin_user
            if user is not None and user not in ulist:
                ulist.append(user)
        return ulist

    @property
    def var_platform(self):
        """
        returns the current value of the platform
        """
        platform = self._vm.platform
        platform_data = None
        if platform is not None:
            platform_data = dict()
            platform_data["value"] = platform.pk
        return platform_data

    @property
    def options_platform(self):
        """
        returns the existing platforms
        """
        platforms_list = list()
        for platform in Platform.objects.all().order_by("team__name", "name"):
            platform_data = dict()
            platform_data["value"] = platform.pk
            team_name = platform.team.name
            platform_name = platform.name
            platform_data["label"] = team_name + ' - ' + platform_name
            platforms_list.append(platform_data)
        return platforms_list

    @property
    def var_folder(self):
        """
        returns the current VM folder
        """
        folder = self.get_inherited_variable("vm_folder")
        folder_data = None
        if folder is not None:
            src, folder = folder
            folder_data = dict()
            folder_data["source"] = src
            folder_data["value"] = folder
        return folder_data

    @property
    def options_folder(self):
        """
        returns a list of folders, following the inheritance tree
        """
        # list all folders
        folders = list()
        teams = Team.objects.all()
        for team in teams:
            folder = team.vm_folder
            if folder is not None and folder not in folders:
                folders.append(folder)
        platforms = Platform.objects.all()
        for platform in platforms:
            folder = platform.vm_folder
            if folder is not None and folder not in folders:
                folders.append(folder)
        machines = Machine.objects.all()
        for machine in machines:
            folder = machine.vm_folder
            if folder is not None and folder not in folders:
                folders.append(folder)
        return folders

    @property
    def var_template(self):
        """
        returns the template currently used for the VM
        """
        tmpl = self.get_inherited_variable("vm_template")
        # get info on the current template
        template = None
        if tmpl is not None:
            src, tmpl = tmpl
            template = dict()
            template["source"] = src
            template["value"] = tmpl.pk
            template["label"] = str(tmpl)
        return template

    @property
    def options_template(self):
        """
        returns a list of available templates
        """
        templates = Template.objects.all().order_by('name')
        templates_list = list()
        for tmpl in templates:
            temp = dict()
            temp['value'] = tmpl.pk
            temp['label'] = str(tmpl)
            templates_list.append(temp)
        return templates_list

    @property
    def var_cpus(self):
        """
        returns the current number of CPUs assigned to the VM
        """
        cpus = self.get_inherited_variable("hw_cpus")
        cpus_dict = None
        if cpus is not None:
            src, cpus = cpus
            cpus_dict = dict()
            cpus_dict["source"] = src
            cpus_dict["value"] = cpus
        return cpus_dict

    @property
    def var_memory(self):
        """
        returns the amount of memory assigned to the VM in MB
        """
        hw_memory = self.get_inherited_variable("hw_memory")
        memory_dict = None
        if hw_memory is not None:
            src, hw_memory = hw_memory
            memory_dict = dict()
            memory_dict["source"] = src
            memory_dict["value"] = hw_memory
        return memory_dict

    @property
    def var_root_size(self):
        """
        returns the size of the VM's root disk in GB
        """
        hw_root = self.get_inherited_variable("hw_root")
        root_dict = None
        if hw_root is not None:
            src, hw_root = hw_root
            root_dict = dict()
            root_dict["source"] = src
            root_dict["value"] = hw_root
        return root_dict

    @property
    def var_root_datastore(self):
        """
        returns the datastore where the VM's root disk is located
        """
        datastore = self.get_inherited_variable("vm_datastore")
        # get info on the current template
        root_datastore = None
        if datastore is not None:
            src, datastore = datastore
            root_datastore = dict()
            root_datastore["source"] = src
            root_datastore["value"] = datastore.pk
            root_datastore["label"] = str(datastore)
        return root_datastore

    @property
    def options_root_datastore(self):
        """
        returns the list of available datastores
        """
        datastores = VMDatastore.objects.all().order_by('name')
        datastores_list = list()
        for datastore in datastores:
            datastore_dict = dict()
            datastore_dict['value'] = datastore.pk
            datastore_dict['label'] = str(datastore)
            datastores_list.append(datastore_dict)
        return datastores_list

    @property
    def var_vlan(self):
        """
        returns the VLAN where the VM's primary interface shows up
        """
        net_vlan = self.get_inherited_variable("net_vlan")
        # get info on the current template
        vlan = None
        if net_vlan is not None:
            src, net_vlan = net_vlan
            vlan = dict()
            vlan["source"] = src
            vlan["value"] = net_vlan.pk
            vlan["label"] = str(net_vlan)
        return vlan

    @property
    def options_vlan(self):
        """
        returns the list of available VLANs
        """
        vlans = Vlan.objects.all().order_by('name').filter(active=True)
        vlans_list = list()
        for vlan in vlans:
            net_vlan = dict()
            net_vlan['value'] = vlan.pk
            net_vlan['label'] = str(vlan)
            vlans_list.append(net_vlan)
        return vlans_list

    @property
    def var_net_ip(self):
        """
        returns the current IP assigned to the network interface
        """
        net_ip = dict()
        net_ip["value"] = self._vm.net_ip
        return net_ip

    @property
    def var_net_gw(self):
        """
        the gw is defined by net_gw and net_pfx_len
        """
        data = dict()
        net_gw = self.get_inherited_variable("net_gw")
        data["net_gw"] = net_gw[1]
        pfx_len = self.get_inherited_variable("net_pfx_len")
        data["net_pfx_len"] = pfx_len[1]
        return data

    @property
    def var_net_search_domains(self):
        """
        returns the search domains
        """
        net_search_domains = self.get_inherited_variable("net_search_domains")
        net_search_domains_dict = None
        if net_search_domains is not None:
            src, net_search_domains = net_search_domains
            net_search_domains_dict = dict()
            net_search_domains_dict["source"] = src
            net_search_domains_dict["value"] = net_search_domains
        return net_search_domains_dict
