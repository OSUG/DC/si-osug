import {IPField} from "./forms-ip-field.js";

export class IPCIDRField extends IPField {
    constructor(form, name, ip_var_name, cidr_var_name) {
        super(form, name, ip_var_name);

        this.control = null;
        this.ip_edit = null;
        this.pfx_edit = null;

        this._net_gw = null;
        this._net_pfx_len = null;

        this.ip_var_name = ip_var_name;
        this.cidr_var_name = cidr_var_name;
        this.get_values();
    };
    get value() {
        return {'ip_addr': this._net_gw, 'pfx_len': this._net_pfx_len};
    }
    values_from_server(data) {
        console.log('IPCIDRField.values_from_server', data);
        if ((data.var !== undefined)&&(data.var !== null)) {
            data = data.var;
            if (data.net_gw !== undefined) {
                this._net_gw = data.net_gw; 
                if (this.ip_edit !== null)
                    this.ip_edit.value = this._net_gw;
            }
            if (data.net_pfx_len !== undefined){
                this._net_pfx_len = data.net_pfx_len;
                if (this.pfx_edit !== null)
                    this.pfx_edit.value = this._net_pfx_len;
            }
        }
    };
    render() {
        super.render();

        var cidr = this;
        
        // remove useless ip control
        if (this.control !== null) {
            console.log("this.control is not null", this.control)
            this.ip_edit = this.control
            this.control = null
        }
        if (this.ip_edit === null) {
            this.ip_edit = document.createElement("input");
            this.ip_edit.type = "text";
            this.field.appendChild(this.ip_edit);
        }
        if (this.ip_edit !== null) {
            this.ip_edit.className = '';
            this.ip_edit.classList.add("cidr-ip-edit"); 
            this.ip_edit.value = this._net_gw;
            this.ip_edit.addEventListener("focusout", () => { cidr.get_ip_value(); });
        }
        if (this.pfx_edit === null) {
            this.pfx_edit = document.createElement("input");
            this.pfx_edit.type = "text";
            this.pfx_edit.classList.add("cidr-pfx-edit");
            this.pfx_edit.value = this._net_pfx_len;
            this.field.appendChild(this.pfx_edit);
            this.pfx_edit.addEventListener("focusout", () => { cidr.get_pfx_len_value(); });
        }
        return this.item; 
    };
    get_ip_value() {
        if (this.ip_edit !== null){
            this._net_gw = this.ip_edit.value;
        }
    };
    get_pfx_len_value() { 
        if (this.pfx_edit !== null)  
            this._net_pfx_len = this.pfx_edit.value;
    };
};
