import {Field} from "./forms-field.js";

export class InheritField extends Field {
    constructor(form, name, varname) {
        super(form, name, varname);

        // dom controls
        this.control = null;
        this.value_line_item = null;
        this.value_item = null;
        this.source_item = null;
        this.change_button = null;
        this.reset_button = null;
        this.options_line_item = null;
        
        // values
        this._orig_value = null;
        this._orig_label = null;
        this._orig_source = null;
        this._value = null;
        this._label = null;
        this._source = null;
        this._options = null;
        this.get_values();
    };  
    get value() {
        return this._value;
    };
    get is_editing() {
        return this.options_line_item.hidden == false;
    }
    values_from_server(values) {
        console.log(this.field_label_text, values);
        if (values.var !== undefined) {
            var server_value = values.var;
            if (server_value !== null) {
                this._source = this._orig_source = server_value.source;
                this.set_values(server_value);
            }
        }
        if (values.options !== undefined)
            this._options = values.options;
        this.update_control();
    };  
    set_values() {
    };
    render_edit_control() {
    };
    render_options() {
    };
    render() {
        super.render();
        var control = this;

        if (this.control === null) {
            this.control = document.createElement('div');
            this.control.classList.add("form-control");

            this.value_line_item = document.createElement("div");
            this.value_line_item.classList.add("form-inherit-info-line");

            this.value_item = document.createElement("span");
            this.value_item.classList.add("form-inherit-value");
            if (this._value !== null)  this.control.textContent = this._value;
            this.value_line_item.appendChild(this.value_item);

            this.source_item = document.createElement("span");
            this.source_item.classList.add("form-inherit-source");
            if (this.source !== null) this.source_item.textContent = this.source;
            this.value_line_item.appendChild(this.source_item);

            this.change_button = document.createElement("span");
            this.change_button.textContent = "🖉";
            this.change_button.classList.add("form-control-button");
            this.change_button.addEventListener("click", () => { 
                control.show_edit_mode();
            });
            this.value_line_item.appendChild(this.change_button);

            this.reset_button = document.createElement('span');
            this.reset_button.textContent = "♻";
            this.reset_button.classList.add("form-control-button");
            this.reset_button.classList.add("hidden");
            this.reset_button.addEventListener("click", () => {
                control.reset_values();
            });
            this.value_line_item.appendChild(this.reset_button);
        
            this.render_edit_control();

            this.control.appendChild(this.value_line_item);
            this.control.appendChild(this.options_line_item);
            this.field.appendChild(this.control);
        }

        return this.item;
    };
    update_control() {
    };
    reset_values() {
        console.log("reset values");
        this._value = this._orig_value;
        this._label = this._orig_label;
        this._source = this._orig_source;
        this.update_control();
        this.reset_button.classList.add("hidden");
    };
    show_edit_mode() {
        this.value_line_item.hidden = true;
        this.options_line_item.hidden = false;
    };
};