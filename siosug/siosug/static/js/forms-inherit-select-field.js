import {InheritField} from "./forms-inherit-field.js";

export class InheritSelectField extends InheritField {
    constructor(form, name, varname) {
        super(form, name, varname);
        this.select_span = null;
        this.select_item = null;
        this.validate_button = null;
        this.cancel_button = null;
    };
    set_values(value) {
        this._value = this._orig_value = value.value;
        this._label = this._orig_label = value.label;
    };
    render_edit_control() {
        var control = this;
        this.options_line_item = document.createElement("div");
        this.options_line_item.classList.add("form-inherit-control-line")
        this.options_line_item.hidden = true;

        this.select_span = document.createElement("span");
        this.select_span.classList.add("select-span");
        this.select_item = document.createElement("select");
        this.select_span.appendChild(this.select_item);
        this.options_line_item.appendChild(this.select_span);

        this.validate_button = document.createElement("span");
        this.validate_button.textContent = "✔";
        this.validate_button.classList.add("form-control-button");
        this.validate_button.addEventListener("click", () => {
            control.validate_change();
        });
        this.options_line_item.appendChild(this.validate_button);

        this.cancel_button = document.createElement("span");
        this.cancel_button.textContent = "✘";
        this.cancel_button.classList.add("form-control-button");
        this.cancel_button.addEventListener("click", () => {
            control.cancel_change();
        });
        this.options_line_item.appendChild(this.cancel_button);
    };
    update_control() {
        this.value_item.textContent = this._label;
        this.source_item.textContent = this._source;
        this.render_options();
    };
    render_options() {
        // empty list of options
        if (this._options === null) return;

        // empty the list
        this.select_item.innerHTML = "";

        // generate an empty option
        var empty_opt = document.createElement("option");
        empty_opt.value = "";
        this.select_item.appendChild(empty_opt);
                
        // add options
        for (var option of this._options) {
            var opt = document.createElement("option");
            opt.value = option.value;
            if (option.value == this._value) opt.selected = true;
            opt.textContent = option.label;
            this.select_item.appendChild(opt);      
        }
    };
    validate_change() {
        this._value = this.select_item.value;
        for (var val of this._options) {
            if (val.value == this._value) {
                this._label = val.label;
                break;
            }
        }
        this._source = null;
        this.update_control();
        this.options_line_item.hidden = true;
        this.value_line_item.hidden = false;
        this.reset_button.classList.remove("hidden");
    };
    cancel_change() {
        // reset the select
        this.select_item.value = this._value;
        this.value_line_item.hidden = false;
        this.options_line_item.hidden = true;
    };
}