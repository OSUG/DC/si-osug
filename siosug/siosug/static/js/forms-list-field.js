import {Field} from "./forms-field.js";

// this is a generic list field

export class ListFieldItem {
    constructor(list_field) {
        this.list_field = list_field;

        this.line = null;
        this.item = null;
        this.remove_button = null;
    };
    get value() {
        return null;
    };
    set value(value) {
    };
    render() {
        if (this.line === null) {
            this.line = document.createElement("div");
            this.line.classList.add("form-list-field-line");
        }

        if (this.item === null) {
            this.item = document.createElement("div");
            this.item.classList.add("form-list-field-item");
            this.line.appendChild(this.item);
        }

        if (this.remove_button === null) {
            var field_item = this;
            this.remove_button = document.createElement('span');
            this.remove_button.textContent = "−";
            this.remove_button.classList.add("form-control-button");
            this.remove_button.addEventListener("click", () => {
                field_item.removeItem();
            });
            this.line.appendChild(this.remove_button);
        }
        return this.line;
    };
    removeItem() {
        this.list_field.remove_item(this);
    };
}

export class ListField extends Field {
    constructor(form, name, varname) {
        super(form, name, varname);
        this.item_class = ListFieldItem;
        this.item_counter = 0;

        this.control = null;
        this.items_list = null;
        this.empty_list_item = null;
        this.control_bar = null;
        this.append_button = null;

        this.items = []; // ListFieldItem
    };
    get value() {
        var _values = [];
        for(var item of this.items) _values.push(item.value);
        return _values;
    };
    values_from_server(data) {
        console.log("ListField.values_from_server", this.varname, data);
        if (data.var !== null)
            this.set_items(data.var.value);
        this.render_items();
    };
    render() {
        super.render();

        if (this.items_list === null) {
            this.items_list = document.createElement("div");
            this.field.appendChild(this.items_list);
        }

        if (this.control_bar === null) {
            this.control_bar = document.createElement("div")
            this.control_bar.classList.add("form-list-field-control-bar");
            this.field.appendChild(this.control_bar);
        }

        if (this.append_button === null) {
            var control = this;
            this.append_button = document.createElement("span");
            this.append_button.textContent = "+";
            this.append_button.classList.add("form-control-button");
            this.append_button.addEventListener("click", () => {
                control.add_item(null);
            });
            this.control_bar.appendChild(this.append_button);    
        }

        return this.item;
    }
    render_items() {
        if (this.items.length == 0) {
            if (this.empty_list_item === null) {
                this.empty_list_item = document.createElement("div");
                this.empty_list_item.classList.add("form-list-field-empty-list");
                this.empty_list_item.textContent = "Aucun élément dans la liste";
            }
            this.items_list.innerHTML = "";
            this.items_list.appendChild(this.empty_list_item);
        } else {    
            this.items_list.innerHTML = "";
            for(var item of this.items) 
                this.items_list.appendChild(item.render());
        }   
    };
    append_item(item) {
        this.items.push(item);
        this.item_counter++;
    }
    add_item(item_data) {
        var new_item = new this.item_class(this, item_data);
        this.append_item(new_item);
        this.render_items();
    };
    set_items(items_data) {
        for(var i in items_data) {
            console.log(this.varname, "set item", i);
            if (i < this.items.length) {
                this.items[i].value = items_data[i];
            } else {
                console.log("    adding new item", items_data[i]);
                this.add_item(items_data[i]);
            }
        }
    };
    get_item_index(item) {
        return this.item_counter;
    };
    remove_item(item) {
        let index = this.items.indexOf(item);
        if (index != -1) {
            this.items.splice(index, 1);
            this.render_items();
        } else console.log("unable to find item in items", item, this.items);
    };
    
}