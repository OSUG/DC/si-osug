import {Field} from "./forms-field.js";

export class SelectField extends Field {
    constructor(form, name, varname) {
        super(form, name, varname);
        this.select_span = null;
        this.control = null;
        this.get_values();  
    };
    get value() {
        console.log(this.control);
        return this.control.value;
    };
    values_from_server(values) {
        if (values.var !== undefined) {
            var value = values.var;
            if (value !== null) {
                this._source = this.orig_source = value.source;
                this.set_values(value);
            }
        }
        if (values.options !== undefined) {
            this._options = values.options;
            this.update_control();
        }
    };  
    set_values(value) {
        this._value = this.orig_value = value.value;
    };
    render() {
        super.render();

        if (this.select_span === null) {
            this.select_span = document.createElement("span");
            this.select_span.classList.add("select-span");
            this.field.appendChild(this.select_span);
        }

        if (this.control === null) {
            var control = this;
            this.control = document.createElement('select');
            this.control.addEventListener("change", () => {
                if (control.on_update !== null) {
                    control.on_update();
                }
            });
            this.control.setAttribute("name", this.varname);
            this.select_span.appendChild(this.control);
        }
        
        return this.item;
    };
    update_control() {
        this.render_options();
    };
    render_options() {
        // empty the options list
        this.control.innerHTML = "";
        
        // generate an empty option
        var empty_opt = document.createElement("option");
        empty_opt.value = "";
        this.control.appendChild(empty_opt);
        
        // add the options from the server
        for (var option of this._options) {
            var opt = document.createElement("option");
            opt.value = option.value;
            opt.textContent = option.label;
            if (this._value == option.value) opt.selected=true;
            this.control.appendChild(opt);
        }
    };
    reset_values() {
        console.log("reset values");
        this._value = this.orig_value;
        this._label = this.orig_label;
        this._source = this.orig_source;
        this.update_control();
        this.reset_button.classList.add("hidden");
    };
    show_edit_mode() {
        this.value_line_item.hidden = true;
        this.options_line_item.hidden = false;
    };
};