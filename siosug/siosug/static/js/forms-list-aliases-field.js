import {ListFieldItem, ListField} from "./forms-list-field.js";

export class ListFieldAliasesItem extends ListFieldItem {
    constructor(list_field, item_data) {
        super(list_field);

        this.item_data = item_data;

        this.radio_button = null;
        this.edit_item = null;
    };
    get value() {
        var main = false;
        if (this.radio_button !== null) main = this.radio_button.checked;
        var alias = null;
        if (this.edit_item !== null) alias = this.edit_item.value;
        var _values = {};
        _values.main = main;
        _values.alias = alias;  
        return _values;
    }
    set value(value) {
        this.item_data = value;
        if (this.radio_button !== null) this.radio_button.checked = this.item_data.main;
        if (this.edit_item !== null) this.edit_item.value = this.item_data.alias;
    }
    render() {
        super.render();
        
        if (this.radio_button === null) {
            this.radio_button = document.createElement("input")
            this.radio_button.setAttribute("type", "radio");
            this.radio_button.setAttribute("name", this.list_field.varname+"_radio");
            this.radio_button.value = this.list_field.get_item_index(this);
            if (this.item_data !== null)
                this.radio_button.checked = this.item_data.main;
            this.item.appendChild(this.radio_button);
        }

        if (this.edit_item === null) {
            this.edit_item = document.createElement('input');
            this.edit_item.setAttribute("type", "text");
            this.edit_item.classList.add("form-list-field-alias-item-edit");
            if (this.item_data !== null)
                this.edit_item.value = this.item_data.alias;
            this.item.appendChild(this.edit_item);   
        }
 
        this.item.classList.add("form-list-field-alias-item");

        return this.line;
    };
};

export class ListAliasesField extends ListField {
    constructor(form, name, varname) {
        super(form, name, varname);
        this.item_class = ListFieldAliasesItem;
        this.get_values();
    };
};