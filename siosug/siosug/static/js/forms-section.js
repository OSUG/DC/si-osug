import {Field} from "./forms-field.js";

export class Section extends Field {
    constructor(form, name, label) {
        super(form, name, null);
        this.item = null;
        this.label = label;
        this.class = "form-section";
    };
    render() {
        if (this.item === null) {
            this.item = document.createElement("div");
            this.item.setAttribute("id", "item_" + this.varname);
            this.item.classList.add(this.class);
            this.item.textContent = this.label;
        }
        return this.item;
    };
};


export class SubSection extends Section {
    constructor(form, name, label) {
        super(form, name, label);
        this.class = "form-sub-section";
    };
};

