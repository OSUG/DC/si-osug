// modal dialog box
// covers the screen with a modal dialog box smack in the middle

export class DialogBox {
    //
    // config is an object with configuration options
    // 
    constructor() {
        this.container_object = null;
        this.bg_container = null;
        this.pane = null;
        this.title_bar = null;
        this.content_pane = null;
        this.footer_pane = null;

        this.title_content = document.createElement('div');
        this.title_content.textContent = "Dialog Box" 
        this.content = document.createElement('div');
        this.buttons_content = document.createElement('div');
        this.buttons_content.classList.add('tb-dialog-buttons');
    }
    set title(value) {
        this.title_content.textContent = value;
    }
    set text(value) {
        this.content.textContent = value;
    };
    set buttons(value) {
        if (Array.isArray(value)) {
            for(var button of value) {
                console.log('button', button);
                var b = document.createElement('button')
                if (button.caption!==undefined) {
                    b.textContent = button.caption
                }
                if (button.onclick!==undefined) {
                    b.addEventListener('click', button.onclick);
                }
                this.buttons_content.appendChild(b);
            }
        }
    };
    run() {
        this.render();
        if (this.container_object===null) {
            // find container object
            this.container_object = document.getElementById("content"); 
        }
        this.container_object.appendChild(this.bg_container);
    };
    render() {
        // does things only if not done yet...
        if (this.bg_container===null) {
            this.bg_container = document.createElement('div');
            this.bg_container.classList.add("tb-dialog-bg-container");
        }
        if (this.pane===null) {
            this.pane = document.createElement('div');
            this.pane.classList.add("tb-dialog-pane");
            this.bg_container.appendChild(this.pane);
        }
        if (this.title_bar===null) {
            this.title_bar = document.createElement('div');
            this.title_bar.classList.add("tb-dialog-title-bar");
            if (this.title_content!==null) {
                this.title_bar.appendChild(this.title_content);
            }
            this.pane.appendChild(this.title_bar);
        }
        if (this.content_pane===null) {
            this.content_pane = document.createElement('div');
            this.content_pane.classList.add("tb-dialog-content-pane");
            if (this.content!==null) {
                this.content_pane.appendChild(this.content);
            }
            this.pane.appendChild(this.content_pane);
        }
        if (this.footer_pane===null) {
            this.footer_pane = document.createElement('div');
            this.footer_pane.classList.add("tb-dialog-footer-pane");
            if (this.buttons_content!==null) {
                this.footer_pane.appendChild(this.buttons_content);
            }
            this.pane.appendChild(this.footer_pane);
        }
    };
    end() {
        if ((this.container_object!==null)&&(this.bg_container!==null)) {
            this.container_object.removeChild(this.bg_container);
        }
    };
}