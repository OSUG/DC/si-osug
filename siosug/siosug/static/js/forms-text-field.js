import {Field} from "./forms-field.js";

export class TextField extends Field {
    constructor(form, name, varname) {
        super(form, name, varname);
        this.control = null;
        this._value = null;
        this.get_values();
    };
    get value() {
        return this.control.value
    };
    values_from_server(values) {
        console.log("TextField", values);
        if (values.var !== undefined)
            this.set_values(values.var);    
        this.update_control();
    };
    set_values(value) {
        if (value !== null)
            this._value = value.value;
    };
    update_control() {
        console.log("text-field value", this._value);   
        this.control.value = this._value;
    };  
    render() {
        super.render();

        if (this.control === null) {
            this.control = document.createElement('input')
            this.control.classList.add('text-field');
            this.control.setAttribute("type", "text");
            this.control.setAttribute("name", this.varname);
            this.field.appendChild(this.control);
        }
        
        return this.item;
    };
};
