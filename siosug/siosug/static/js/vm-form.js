import * as tb from "./toolbox.js";
/*
  * vm name
  * aliases list with one name selected
  * administrative user name
# platform
  * platform
  * folder
  * template
# hardware
  * nb cpus
  * memory
  # disk
    * disk size
    * datastore
# network
  * vlan
  * ip (cidr)
  * gateway
  * search domains
# packages
  * system
  * pip
*/

function get_vm_id() {
    let path = window.location.pathname
    let re = /\/manage\/vm\/(new|\d+)/;
    let matches = path.match(re)
    let vm_id = matches[1];
    if (vm_id!=="new") vm_id = parseInt(vm_id);
    console.log(vm_id);
    return vm_id;
}

function form_url() {
    return window.location.pathname;
};

function variable_url(variable_name) {   
    return "/manage/vm/"+get_vm_id()+"/"+variable_name+"/";
};

var form = new tb.Form("vm-form", form_url, variable_url);

var vm_name = new tb.TextField(form, "Nom de la VM", "vm_name");
var dns_aliases = new tb.ListAliasesField(form, "Liste des Alias DNS", "dns_aliases");
var admin_login = new tb.InheritTextField(form, "login administrateur", "admin_login");
var platform = new tb.SelectField(form, "Plateforme", "platform");
var folder = new tb.InheritTextField(form, "Dossier", "folder");
var template = new tb.InheritSelectField(form, "Template", "template");
var cpus = new tb.InheritTextField(form, "Nombre CPUs", "cpus");
var memory = new tb.InheritTextField(form, "Taille Mémoire (Mo)", "memory");
var root_size = new tb.InheritTextField(form, "Taille (Go)", "root_size");
var root_datastore = new tb.InheritSelectField(form, "Emplacement", "root_datastore");
var vlan = new tb.InheritSelectField(form, "Vlan", "vlan");
var net_ip = new tb.IPField(form, "Adresse IP", "net_ip");
var net_gw = new tb.IPCIDRField(form, "Passerelle reseau", "net_gw");
var net_search_domains = new tb.InheritTextField(form, "Liste de domaines de recherche", "net_search_domains");  

platform.on_update = () => {
    console.log("updating platform");
};

form.on_success = () => {
    var f = form;
    return (data) => {
        //
        // something really bad happened (some data was wrong or forgotten)
        //
        if ((data.ok !== undefined)&&(data.ok===false)) {   
            f.set_errors(data.errors)();
            return;
        }
        // 
        // a vm with the same name already existed
        //
        if (data.already_exists !== undefined) {
            console.log("VM already exists");
            var dialog = new tb.DialogBox();
            dialog.title = "Une erreur s'est produite"
            dialog.text = "Une machine virtuelle existe déjà avec le nom '"+vm_name.value+"'";
            dialog.buttons = [
                {
                    caption: "Éditer la VM existante",
                    onclick: () => {
                        console.log("replacing location", data.already_exists)
                        history.replaceState(null, "", "/manage/vm/" + data.already_exists);
                        form.reload();
                        dialog.end();
                    }
                },
                {
                    caption: "Continuer l'édition",
                    onclick: () => {
                        dialog.end();
                    }
                },
            ];
            dialog.run();
            return;
        }
        console.log("VM did not exist initially");
        if (data.redirect_to !== undefined)
            window.location.pathname = data.redirect_to;
    };
};

form.fields = [
    new tb.Section(form, "section-base-infos", "Description de la VM"),
    vm_name, dns_aliases, admin_login,
    new tb.Section(form, "section-platform", "Plateforme"),
    platform, folder, template,
    new tb.Section(form, "section-hardware", "Matériel"),
    cpus, memory,
    new tb.SubSection(form, "subsection-root", "Disque Principal"),
    root_size, root_datastore,
    new tb.Section(form, "section-network", "Réseau"),
    vlan, net_ip, net_gw, net_search_domains,
    new tb.Section(form, "section-packages", "Paquets à installer"),
    new tb.SubmitButton(form, "submit")
];

form.render();      