export class Ajax {
    constructor() {
        this._url = null;
        this._method = "GET";
        this._data = null;
        this._success = null;
        this._error = null;
        this._headers = null;
        this._req = new XMLHttpRequest();
        var self = this;
        this._req.onreadystatechange = () => {
            self.state_change();
        };
    };
    set url(url_string) {
        this._url = url_string;
    };
    set method(method_name) {
        this._method = method_name;
    };
    set data(data) {
        this._data = data;
    };
    set type(response_type) {
        this._req.responseType = response_type;
    };
    set success(success_func) {
        this._success = success_func;
    };
    set error(error_func) {
        this._error = error_func;
    };
    add_header(header, content) {
        if (this._headers === null) {
            this._headers = {};
        }
        this._headers[header] = content;
    };
    state_change() {
        switch (this._req.readyState) {
            case XMLHttpRequest.OPENED: 
                break;
            case XMLHttpRequest.LOADING:
                break;
            case XMLHttpRequest.HEADERS_RECEIVED:
                break;
            case XMLHttpRequest.DONE:
                if (this._req.status == 200) {
                    if (this._success !== null) {
                        this._success(this._req.response);
                    }
                }
                if (this._req.status>=400) {
                    if (this._error !== null) {
                        this._error(this._req);
                    }
                }
                break;
        }
    };
    run() {
        this._req.open(this._method, this._url);
        if (this._headers !== null) {
            for(var h in this._headers) {
                console.log(h);
                this._req.setRequestHeader(h, this._headers[h]);
            }
        }
        if (typeof(this._data) == "object") {
            this._data = JSON.stringify(this._data);
            this._req.setRequestHeader("Content-Type", "application/json");
        }
        this._req.send(this._data);
    };
}
