import {Ajax} from "./ajax.js";

export class Field {
    constructor(form, label, varname) {
        this.form = form;
        this.field_label_text = label;
        this.varname = varname;

        // controls
        this.item = null;
        this.label = null;
        this.field = null;

        this.on_update = null;

        this.errmsg = null;
        this.err_item = null;
    };
    get url(){
        return this.form.var_url_func(this.varname);
    }
    get value() {
        return this._value;
    }
    get is_editing() {
        return false;
    };
    get_values() {
        if (this.varname===null) return;
        var f = this;
        var a = new Ajax();
        a.url = this.url;
        a.type = "json";
        a.success = (data) => {
            f.values_from_server(data);
        };
        a.run();
    };
    values_from_server(data) {
        console.log(data);
    };
    render() {
        if (this.item === null) {
            this.item = document.createElement("div");
            this.item.classList.add("form-line");
        }
        
        if (this.label === null) {
            this.label = document.createElement("label");
            this.label.classList.add("form-line-label");
            this.label.textContent=this.field_label_text;
        }

        if (this.field === null) {
            this.field = document.createElement("div");
            this.field.classList.add("form-line-control");
        }

        this.item.innerHTML = "";
        this.item.appendChild(this.label);
        this.item.appendChild(this.field);
        return this.item;
    };
    set_errors(errors) {
        if (this.varname === null)
            this.errmsg = null;
        else {
            let errmsg = errors[this.varname];
            if (errmsg !== undefined) this.errmsg = errmsg;
            else this.errmsg = null;
        }
        this.display_error();
    };
    display_error() {
        if (this.errmsg === null) {
            if (this.err_item) {
                console.log(this.varname+" - remove error message");
                this.err_item.textContent = "";
            }
        } else {
            console.log(this.varname + ' display_error \'' + this.errmsg + '\'');
            if (this.err_item === null)
                this.err_item = document.createElement("span");
            this.err_item.textContent = this.errmsg;
            this.field.appendChild(this.err_item);
        }
    };
}
