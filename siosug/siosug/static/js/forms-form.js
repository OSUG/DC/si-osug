import {Ajax} from "./ajax.js";

export class Form {
    constructor(container, form_url_func, var_url_func) {
        this._fields = null;
        this.container = container;
        this._form_url_func = form_url_func;
        this._var_url_func = var_url_func;
        this.on_success = null;
    };
    set fields(fields) {
        this._fields = fields;
    };
    get var_url_func() {
        return this._var_url_func;
    }
    reload() {
        // reloads all fields of the form after the URL was changed
        for(var field of this._fields) {
            if (field.get_values!==undefined) {
                if (field.var_name === null) console.log("field null", field);
                else field.get_values();
            }
        }
    };
    render() {
        // generates the form in the container
        var form = document.createElement('form');

        for(var field of this._fields) {
            // console.log(field);
            var field_item = field.render()
            // console.log(field_item);
            form.appendChild(field_item);
        }

        let container = document.getElementById(this.container);
        container.innerHTML = "";
        container.appendChild(form);
    }
    submit() {
        console.log("submitting form");
        var editing_list = [];
        for(var field of this._fields) if (field.is_editing) editing_list.push(field);
        console.log(editing_list.length, editing_list);
        if (editing_list.length > 0) {
            // TODO: dialog box
            var response = confirm("Vous avez "+editing_list.length+" champ(s) non validés,\n"+
                "* continuer d'éditer le formulaire (cancel),\n"+
                "* ou valider les champs (ok) ?");
            console.log(response);
            if (!response) return;
            for(var field of this._fields) {
                if (field.validate_change !== undefined) field.validate_change();
            }
        }

        var data = {};
        for(var field of this._fields) {
            if (field.varname !== null) {
                data[field.varname] = field.value;
            }
        }
        console.log(data);
        var f = this;
        var a = new Ajax();
        a.url = this._form_url_func();
        a.method = "POST";
        a.data = data;
        a.type = "json";

        // traitement du token CSRF pour le post
        var tok_input = document.getElementsByName('csrfmiddlewaretoken');
        if (tok_input.length >= 1) {
            tok_input = tok_input[0];
            let csrf_token = tok_input.value;
            a.add_header("X-CSRFToken", csrf_token);
        }
        
        a.success = (data) => {
            f.form_post_success(data);
        };
        a.run();  
    };
    form_post_success(data) {
        console.log(data);
        if (this.on_success !== null) {
            this.on_success()(data);
        }
    };
    set_errors(errors) {
        var f = this;
        var e = errors;
        return function() {
            for(var field of f._fields) {
                field.set_errors(e);
            }
        };
    };
};
