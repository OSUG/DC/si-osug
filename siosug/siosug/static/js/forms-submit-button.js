import {Field} from "./forms-field.js";

export class SubmitButton extends Field {
    constructor(form, label_text) {
        super(form, "", null)
        this.form = form;
        this.label_text = label_text;
        this.button = null;
    };
    render() {
        super.render()
        var button_ctrl = this;

        if (this.button === null) {
            this.button = document.createElement("button");
            this.button.setAttribute("type", "button");
            this.button.classList.add("form-submit-button");
            this.button.textContent = this.label_text;
            this.button.addEventListener("click", () => {
                button_ctrl.clicked();
            });
        }

        this.field.innerHTML = ""
        this.field.classList.add("form-line-submit");
        this.field.appendChild(this.button);
        return this.item;
    };
    clicked() {
        console.log("submit button for", this.form, "clicked");
        this.form.submit();
    };
};