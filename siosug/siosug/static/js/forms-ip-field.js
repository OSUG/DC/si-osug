import {TextField} from "./forms-text-field.js";

export class IPField extends TextField {
    constructor(form, name, varname) {
        super(form, name, varname);
    };
};
