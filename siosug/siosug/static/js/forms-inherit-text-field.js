import {InheritField} from "./forms-inherit-field.js";

export class InheritTextField extends InheritField {
    constructor(form, name, varname) {
        super(form, name, varname);
        this.edit_item = null;
        this.datalist_item = null;
        this.validate_button = null;
        this.cancel_button = null;
    };
    set_values(value) {
        this._value = this._orig_value = value.value;
    };
    render_edit_control() {
        var control = this;
        this.options_line_item = document.createElement("div");
        this.options_line_item.classList.add("form-inherit-control-line")
        this.options_line_item.hidden = true;

        var datalist_id = "datalist_" + this.varname;

        this.edit_item = document.createElement("input");
        this.edit_item.type = "text"; 
        this.edit_item.setAttribute("list", datalist_id);
        this.edit_item.value = this._value;  
        this.options_line_item.appendChild(this.edit_item);

        this.datalist_item = document.createElement("datalist");
        this.datalist_item.setAttribute("id", datalist_id);
        this.options_line_item.appendChild(this.datalist_item);

        this.validate_button = document.createElement("span");
        this.validate_button.textContent = "✔";
        this.validate_button.classList.add("form-control-button");
        this.validate_button.addEventListener("click", () => {
            control.validate_change();
        });
        this.options_line_item.appendChild(this.validate_button);

        this.cancel_button = document.createElement("span");
        this.cancel_button.textContent = "✘";
        this.cancel_button.classList.add("form-control-button");
        this.cancel_button.addEventListener("click", () => {
            control.cancel_change();
        });
        this.options_line_item.appendChild(this.cancel_button);
    };
    update_control() {
        this.value_item.textContent = this._value;
        this.source_item.textContent = this._source;
        this.render_options();
    };
    render_options() {
        this.edit_item.value = this._value;
        // empty list of options
        if (this._options === null) return;
        this.datalist_item.innerHTML = "";
        for (var option of this._options) {
            var opt = document.createElement("option");
            opt.value = option;
            if (option == this._value) opt.selected = true;
            this.datalist_item.appendChild(opt);      
        }
    };
    validate_change() {
        this._value = this.edit_item.value;
        this._source = null;
        this.update_control();
        this.options_line_item.hidden = true;
        this.value_line_item.hidden = false;
        this.reset_button.classList.remove("hidden");
    };
    cancel_change() {
        // reset the select
        this.edit_item.value = this._value;
        this.value_line_item.hidden = false;
        this.options_line_item.hidden = true;
    };
}