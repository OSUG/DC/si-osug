# Generated by Django 2.1 on 2019-02-04 10:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('siosug', '0008_usergroup'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='admin_flag',
            field=models.BooleanField(default=False),
        ),
    ]
