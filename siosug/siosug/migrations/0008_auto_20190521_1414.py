# Generated by Django 2.2.1 on 2019-05-21 14:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('siosug', '0007_auto_20190521_1411'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='ssh_pubkeys',
            field=models.ManyToManyField(blank=True, related_name='user', to='siosug.SSHKey'),
        ),
    ]
