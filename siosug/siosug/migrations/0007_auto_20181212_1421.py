# Generated by Django 2.1 on 2018-12-12 14:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('siosug', '0006_auto_20181212_1419'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vmdisk',
            name='size_b',
            field=models.BigIntegerField(),
        ),
    ]
