# -*- coding: utf-8 -*-
"""
Authentication backend for OSUG's requirements
"""

import logging

import ldap3
from django.conf import settings
from django.db.utils import IntegrityError

from ..models import User


class OsugAuthenticationBackend:
    def __init__(self):
        print("OsugAuthenticationBackend")
        self.logger = logging.getLogger("django.request")
        # self.logger.critical("------ TEST FROM OsugAuthenticationBackend __init__ ------")

    """
    Django authentication backend specific to OSUG's infrasctructure
    """

    def authenticate(self, request, username=None, password=None):  # pylint: disable=W0613
        """
        Authenticates a user from the database or the LDAP server
        If the user is not in the database, adds it there
        """
        # start with making sure we have no user
        user = None

        # self.logger.critical("------ authenticate : start ------")
        #self.logger.error("OsugAuthenthcationBackend.authenticate %s %s" % (username, password))
        # auth from the database ?
        try:
            # self.logger.debug("Trying to get user with username %s"%(username))
            user = User.objects.get(username=username)
            # self.logger.debug("user obtained successfully, checking password")
            # check password
            if user.is_password_valid(password):
                # self.logger.debug("user used a valid password")
                # user exists and password id correct
                return user
        except User.DoesNotExist:
            self.logger.error("User '%s' does not exist in the database, "
                              "looking in the ldap server", username)
        
        # self.logger.critical("------ authenticate : trying LDAP ------")
        # authenticate with ldap
        server = ldap3.Server(settings.OSUG_LDAP_SERVER, get_info=ldap3.ALL, connect_timeout=2)
        conn = ldap3.Connection(server)
        try:
            conn.open()
        except ldap3.core.exceptions.LDAPSocketOpenError:
            self.logger.critical("Unable to connect to LDAP server")
            return None
        conn.start_tls()
        # TODO: loop on all branches
        osug_people_base = 'ou=people,ou=osug-ums,dc=osug,dc=fr'
        user_dn = 'uid=%s,%s'%(username, osug_people_base)
        self.logger.info("dn '%s'"%(user_dn))


        if conn.rebind(user=user_dn, password=password):
            # fetch user information
            conn.search(osug_people_base, '(uid=%s)'%(username), attributes=['uidNumber'])
            user_data = conn.entries[0]
            uidNumber = user_data.uidNumber.values[0]
            # print("uidNumber : %d"%(uidNumber))
            # self.logger.error("user is valid")
            
            # search if we have a user with that uidNumber

            # must return an object
            if user is None:
                user = User()
                user.unix_uid = uidNumber
                user.username = username
            user.set_password(password)
            try:
                user.save()
            except Exception as e:
                self.logger.critical(e)
                return None
            return user
        else:
            self.logger.error("user invalid")
            return None

    def get_user(self, user_id):
        """
        returns the user with the given ID (primary key)
        """
        #logger = logging.getLogger(self.__class__.__name__)
        logger = logging.getLogger("django.request")
        logger.error("OsugAuthenticationBackend.get_user %d", user_id)
        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            logger.error("User not found")
            return None
        logger.error("found user %s", str(user))
        return user
