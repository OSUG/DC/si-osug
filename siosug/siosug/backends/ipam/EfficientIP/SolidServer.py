# pylint: disable=invalid-name
import base64
import ipaddress
from urllib.parse import quote_plus

import requests
from django.conf import settings


def dump_dict(data):
    """
    debug function, dumps the contents of a dict
    """
    for key in data.keys():
        print(key, data[key])

class SolidServerNoHostSpecified(Exception):
    def __init__(self):
        super().__init__("SolidServer: No HOST specified")

class SolidServerNoUserSpecified(Exception):
    def __init__(self):
        super().__init__("SolidServer: No HOST specified")

class SolidServerNoPasswordSpecified(Exception):
    def __init__(self):
        super().__init__("SolidServer: No HOST specified")

class IpamManager:
    def __init__(self, config):
        self.server_name = config.get("HOST", None)
        if not self.server_name:
            raise SolidServerNoHostSpecified
        print("has HOST '%s'"%(self.server_name))
        self.username = config.get("USER", None)
        if not self.username:
            raise SolidServerNoUserSpecified
        print("has USER '%s'"%(self.username))
        self.password = config.get("PASSWORD", None)
        if not self.password:
            raise SolidServerNoPasswordSpecified
        print("has PASSWORD  '%s'"%(self.password))

    def api_call(self, method, api_name, params=None):
        # generate url
        url = "https://"
        url += self.server_name
        url += "/rest/"
        url += api_name

        if params is not None:
            if method == "GET":
                # encode parameters
                parameters = []
                for p_name in params.keys():
                    parameters.append(p_name)
                    parameters.append(quote_plus(params[p_name]))

                url += '/'+ '/'.join(parameters)

        b64_username = base64.b64encode(self.username.encode('utf-8'))
        b64_password = base64.b64encode(self.password.encode('utf-8'))
        headers = {
            "X-IPM-Username": b64_username,
            "X-IPM-Password": b64_password
        }
        response = requests.request(
            method, 
            url,
            headers=headers)
        if response.status_code == 200:
            json_data = response.json()
            return json_data
        print("response %d"%(response.status_code))
        return None


    def get_ip_id(self, ip_address):
        ip_addr = ipaddress.ip_address(ip_address)
        ip_addr_str = ''
        for b in ip_addr.packed:
            ip_addr_str += '%02x'%(b)
        ip_data = self.api_call('GET', 'ip_address_list', {"WHERE": "ip_addr='%s'"%(ip_addr_str)})
        if ip_data is None:
            return None
        if len(ip_data) == 1:
            ip_data = ip_data[0]
            return ip_data['ip_id']
        dump_dict(ip_data[0])
        return None

    def ip_addr_aliases(self, ip_address):
        print('looking up aliases for %s'%(ip_address))
        ip_id = self.get_ip_id(ip_address)
        print('ip_id %s'%(str(ip_id)))
        if ip_id is None:
            return None
        ip_aliases = self.api_call('GET', 'ip_alias_list', {'ip_id': ip_id})
        if ip_aliases is None:
            return None
        aliases_list = []
        for ip_alias in ip_aliases:
            alias = ip_alias['alias_name']
            aliases_list.append(alias)
        return aliases_list    

