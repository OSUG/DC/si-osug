import importlib

from django.conf import settings


class IPAMNotAvailable(Exception):
    def __init__(self):
        super().__init__("IPAM unavailable: no IPAM defined in settings.py")

class NoIPAMEngineDefined(Exception):
    def __init__(self):
        super().__init__("IPAM unavailable: no IPAM engine defined in settings.py")

class IpamManager:
    def __init__(self):
        self.ipam_config = getattr(settings,"IPAM", None)
        if not self.ipam_config:
            raise IPAMNotAvailable
        self.ipam_module_path = self.ipam_config.get("ENGINE")
        if not self.ipam_module_path:
            raise NoIPAMEngineDefined
        self.ipam_module = importlib.import_module(self.ipam_module_path)
        self.ipam = self.ipam_module.IpamManager(self.ipam_config)

    def ip_addr_aliases(self, ip_address):
        if not self.ipam:
            raise IPAMNotAvailable
        return self.ipam.ip_addr_aliases(ip_address)
