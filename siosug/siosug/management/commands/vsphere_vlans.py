"""
This module fetches all vlans existing in the vsphere environment
and inserts them in the django database
"""

from django.conf import settings
from django.core.management.base import BaseCommand  # , CommandError

from ...models import Vlan
from ._vmware_tools import VsphereConnexion


class Command(BaseCommand):
    """
    Command object for vsphere_vlans command
    """
    def handle(self, *args, **options):
        vsconn = VsphereConnexion()
        if settings.DEBUG:
            print('successfully connected')
        port_groups = vsconn.port_groups()
        # if settings.DEBUG:
        #     print(port_groups)
        # create new vlans
        vlan_list = []
        if settings.DEBUG:
            print("Adding new Vlans :")
        for vlan_name in port_groups:
            vlan_list.append(vlan_name)
            try:
                vlan = Vlan.objects.get(name=vlan_name)
            except Vlan.DoesNotExist:
                # print('    vlan does not exist')
                vlan = Vlan()
                vlan.name = vlan_name
                vlan.save()
                if settings.DEBUG:
                    print("    %s"%(vlan_name))
            else:
                if not vlan.active:
                    vlan.active = True
                    vlan.save()
        # remove vlans that are gone
        vlans = Vlan.objects.all()
        if settings.DEBUG:
            print("Removing Vlans :")
        for vlan in vlans:
            if vlan.name not in vlan_list and vlan.active:
                print("vlan '%s' gone"%(vlan.name))
                vlan.active = False
                vlan.save()
                if settings.DEBUG:
                    print("    %s"%(vlan.name))