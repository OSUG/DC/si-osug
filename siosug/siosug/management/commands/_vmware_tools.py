"""
this module centralizes access to vsphere stuff for django commands
"""

import ssl

import yaml
from django.conf import settings
try:
    from pyVim.connect import SmartConnect
except:
    from pyvim.connect import SmartConnect

from pyVmomi import vim  # pylint: disable=E0611


class VsphereConnexion():
    """
    class with utility functions to access vsphere stuff
    """

    login = None
    passwd = None


    def __init__(self):

        if not self.load_login():
            print("error loading VSphere logins")
            return

        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        ssl_context.verify_mode = ssl.CERT_NONE
        smart_connect = SmartConnect(host="osug-vcenter6.ujf-grenoble.fr",
                                     user=self.login,
                                     pwd=self.passwd,
                                     sslContext=ssl_context)
        self.vsphere_content = smart_connect.content

    def load_login(self):
        """
        load login info from the ansible settings, pointed to by the django settings
        """
        logins = open(settings.ANSIBLE_LOGINS, 'r')
        try:
            yaml_data = yaml.load(logins, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)
            return False
        self.login = yaml_data.get('vsphere_login', None)
        self.passwd = yaml_data.get('vsphere_passwd', None)
        return True

    def objects(self, vimtype):
        """
        returm all objects of a type
        """
        root_folder = self.vsphere_content.rootFolder
        view_manager = self.vsphere_content.viewManager
        container = view_manager.CreateContainerView(root_folder, vimtype, True)
        return container.view

    def get_all_objs(self, vimtype):
        """
        return all objects of a certain type
        """
        obj = {}
        root_folder = self.vsphere_content.rootFolder
        container = self.vsphere_content.viewManager.\
                                         CreateContainerView(root_folder,
                                                             vimtype,
                                                             True)
        for managed_object_ref in container.view:
            obj.update({managed_object_ref: managed_object_ref.name})
        return obj

    def port_groups(self):
        """
        return all port groups (vlans) in the vsphere cluster
        """

        all_dvs = self.get_all_objs([vim.dvs.VmwareDistributedVirtualSwitch])

        _port_groups = []

        for dvs in all_dvs:
            print(dvs.name)
            for port in dvs.portgroup:
                if port.config.name not in _port_groups:
                    print("    %s"%(port.config.name))
                    _port_groups.append(port.config.name)            
        return _port_groups

    def virtual_machines(self):
        """
        returns a list of all virtual machines
        """
        all_vms = self.objects([vim.VirtualMachine])

        virtual_machines = dict()
        for v_machine in all_vms:
            if v_machine is None:
                continue
            names = []
            parent = v_machine.parent
            while parent:
                # stop when parent is "vm"
                if parent.name == 'vm':
                    break
                names.insert(0, parent.name)
                parent = parent.parent
            vm_dir_path = "/".join(names)
            if vm_dir_path:
                vm_dir_path = '/' + vm_dir_path
            vm_data = dict()
            vm_data['dir_path'] = vm_dir_path
            vm_data['vm'] = v_machine
            vm_complete_path = vm_dir_path + "/" + v_machine.name
            virtual_machines[vm_complete_path] = vm_data

        return virtual_machines
