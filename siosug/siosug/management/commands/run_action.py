"""
This module attempts to execute ActionRecords
"""

import ipaddress
import socket

from django.conf import settings
from django.core.management.base import BaseCommand  # , CommandError

from siosug.models import ActionRecord

class Command(BaseCommand):
    """
    class executing ActionRecords when there are some present that need be handled
    """
    def handle(self, *args, **options):
        next_action = ActionRecord.objects.filter(state=ActionRecord.AS_TODO).order_by('id')[0]
        if next_action: 
            next_action.run()