"""
This module inserts all VMs from the old ansible inventory into the
django database
"""

import os
import os.path
# import ipaddress
import re
import subprocess
import tempfile

import yaml
from django.conf import settings
from django.core.management.base import BaseCommand  # , CommandError

from ...models import Team, Platform, Machine  # , Template, Vlan, VMDatastore, VMGuestOs


class Command(BaseCommand):
    """
    Command object for the ansible_import_vms command
    """

    def clone_ansible_inventory(self):
        """
        git clone the repository into /tmp/random_dir
        """
        self.temp_dir = tempfile.TemporaryDirectory(prefix="ansible_import_vms_")
        # do stuff with directory
        self.dir_name = self.temp_dir.name
        command = ['git', 'clone', '--', settings.OLD_ANSIBLE_REPO, self.dir_name]
        print(command)
        # run git command
        run_result = subprocess.run(command)
        print(run_result)

    def extract_data(self):
        """
        extract the data from the ansible configuration
        """

        # find teams
        base_dir = os.path.abspath(self.dir_name)
        print(base_dir)
        platforms_dir = os.path.join(base_dir, 'ansible', 'teams')
        print(platforms_dir)

        # loop on each platform directory

        for platform_dir in os.scandir(platforms_dir):
            print(platform_dir)
            platform_name = platform_dir.name
            print(platform_name)

            # find platform
            _platform_changed = False
            try:
                _platform = Platform.objects.get(name=platform_name)
            except Platform.DoesNotExist:
                # try to guess then
                names = platform_name.split('-')
                if len(names) == 1:
                    # there is no "-", we have only one name, let's consider (team-default)
                    # set names to allow creating the team with a default platform
                    names = (platform_name, "default",)
                if len(names) == 2:
                    team_name, platform_name = names
                    try:
                        _team = Team.objects.get(name=team_name)
                    except Team.DoesNotExist:
                        print("team %s does not exist (platform %s)"%(team_name, platform_name))
                        # create team then
                        _team = Team()
                        _team.name = team_name
                        _team.save()
                    _platforms = Platform.objects.filter(team=_team)
                    if len(_platforms) == 0:
                        print("there are no platforms for team %s"%(team_name))
                    try:
                        _platform = _platforms.get(name=platform_name)
                    except Platform.DoesNotExist:
                        print("there is no platform with name %s in team %s"%(platform_name, team_name))
                        # create platform
                        _platform = Platform()
                        _platform.team = _team
                        _platform.name = platform_name
                        # add users ??
                        #_platform.save()
                        _platform_changed = True
                else:
                    print("there are %d name elements %s"%(len(names), str(names)))
                    break

            vm_spec_path = os.path.join(platform_dir.path, 'vm-spec.yml')
            print(vm_spec_path)

            # the configuration is stored in a yaml file
            vm_spec_file = open(vm_spec_path, 'r')
            try:
                vm_spec_data = yaml.load(vm_spec_file)
            except yaml.YAMLError as exc:
                print(exc)
                continue

            platform_vars = dict()
            for vm_spec_var in vm_spec_data.keys():
                if vm_spec_var != 'vm_list':
                    # register variable
                    platform_vars[vm_spec_var] = vm_spec_data[vm_spec_var]
            print("platform_vars ", platform_vars)

            # update some platform vars
            _platform_changed = False

            admin_user = platform_vars.get('admin_user', None)
            if admin_user and _platform.admin_user != admin_user:
                _platform.admin_user = admin_user
                _platform_changed = True

            if _platform_changed:
                _platform.save()

            # function to resolve variables
            def get_data(data, var_name):
                value = None
                if data is not None:
                    value = data.get(var_name, None)
                if value is not None and type(value) is str:
                    # check for "{{stuff}}" form
                    matches = re.match(r'^\{\{(.*)\}\}$', value)
                    if matches:
                        var_name = matches.groups()[0]
                        if var_name not in platform_vars:
                            print("unable to find variable '%s'"%(var_name))
                        else:
                            var_value = platform_vars[var_name]
                            value = var_value
                return value

            # get the list of VMs from the configuration
            vm_list = vm_spec_data['vm_list']

            #
            # for all vms defined, extract data
            #
            for _vm_name in vm_list.keys():
                vm_data = vm_list[_vm_name]
                _vm_name_ = get_data(vm_data, 'name')
                _vm_template = get_data(vm_data, 'template')
                _vm_guest_id = get_data(vm_data, 'guest_id')
                _vm_folder = get_data(vm_data, 'folder')
                _vm_hostname = get_data(vm_data, 'hostname')
                _vm_domain_names = get_data(vm_data, 'domain_names')
                _vm_datastore = get_data(vm_data, 'datastore')

                _vm_hardware = get_data(vm_data, 'hardware')
                _vm_cpus = get_data(_vm_hardware, 'cpus')
                _vm_memory = get_data(_vm_hardware, 'memory')
                _vm_root_size = get_data(_vm_hardware, 'root_size')
                
                _vm_ip_config = get_data(vm_data, 'ip_config')
                _vm_address = get_data(_vm_ip_config, 'address')
                _vm_netmask = get_data(_vm_ip_config, 'netmask')
                _vm_gateway = get_data(_vm_ip_config, 'gateway')
                _vm_net_name = get_data(_vm_ip_config, 'net_name')
                _vm_search_domains = get_data(_vm_ip_config, 'search_domains')

                _vm_install = get_data(vm_data,'install')
                _vm_install_packages = get_data(_vm_install, 'packages')
                _vm_install_pip = get_data(_vm_install, 'pip')

                _vm_syslog_server = get_data(vm_data, 'syslog_server')
                _vm_ansible_master = get_data(vm_data, 'ansible_master')

                #
                # print out the information (used for debugging)
                #

                print('    %s'%(_vm_name))
                # print('         name               : %s'%(_vm_name_))
                # print('         template           : %s'%(_vm_template))
                # print('         guest_id           : %s'%(_vm_guest_id))
                # print('         folder             : %s'%(_vm_folder))
                # print('         hostname           : %s'%(_vm_hostname))
                # print('         domain names       : %s'%(_vm_domain_names))
                # print('         datastore          : %s'%(_vm_datastore))

                # print('         hardware           : %s'%(_vm_hardware))
                # print('             cpus           : %s'%(_vm_cpus))
                # print('             memory         : %s'%(_vm_memory))
                # print('             root_size      : %s'%(_vm_root_size))
                
                # print('         ip_config          : %s'%(_vm_ip_config))
                # print('             address        : %s'%(_vm_address))
                # print('             netmask        : %s'%(_vm_netmask))
                # print('             gateway        : %s'%(_vm_gateway)) 
                # print('             net_name       : %s'%(_vm_net_name))
                # print('             search_domains : %s'%(_vm_search_domains))

                # print('         install            : %s'%(_vm_install))
                # print('             packages       : %s'%(_vm_install_packages))
                # print('             pip            : %s'%(_vm_install_pip))
                # print('         syslog_server      : %s'%(_vm_syslog_server))
                # print('         ansible_master     : %s'%(_vm_ansible_master))
                
                # time to find an existing machine
                
                # update the related vm_object entry
                try:
                    vm_object = Machine.objects.get(vm_name=_vm_name)
                    is_new = False
                except Machine.DoesNotExist:
                    print("no machine named %s"%(_vm_name))
                    is_new = True
                    continue

                def update(attr, value, _messages=None, _vm_object=vm_object, _is_new=is_new):
                    old_value = getattr(_vm_object, attr)
                    if _is_new or old_value != value:
                        _messages.append("+ '%s' : %s"%(attr, str(value)))
                        setattr(_vm_object, attr, value)
                        return True
                    _messages.append(". '%s' : %s"%(attr, str(value)))
                    return False

                changed = False
                messages = list()

                changed |= update("platform", _platform, messages)
                # changed |= update("admin_user", _vm_admin_user, messages)

                # if the vm_object was changed, save it
                if changed:
                    print("\n".join(messages))
                    vm_object.save()

        # destroy the temporary directory
        self.temp_dir.cleanup()

    # this function is what is called by the Django manage.py script
    def handle(self, *args, **options):
        self.clone_ansible_inventory()
        self.extract_data()
