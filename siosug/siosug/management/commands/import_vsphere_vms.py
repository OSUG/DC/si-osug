"""
This module inserts all VMs from the vsphere environment into the
django database
"""

import ipaddress
import socket

from django.conf import settings
from django.core.management.base import BaseCommand  # , CommandError

from siosug.backends.ipam import IpamManager

from ...models import Machine, Template, Vlan, VMDatastore, VMDisk, VMGuestOs
from ._vmware_tools import VsphereConnexion


class Command(BaseCommand):
    """
    Command object for the vsphere_import_vms command
    """
    def handle(self, *args, **options):
        vsconn = VsphereConnexion()
        if settings.DEBUG:
            print('successfully connected')

        # generates a list of vms with folders

        # obtain the list of VM from vmware
        virtual_machines = vsconn.virtual_machines()

        # lists of vm for various error conditions
        no_guestid = list()
        unknown_guestos = dict()
        too_many_disks = dict()
        no_network = list()
        too_many_networks = dict()
        unspecified_vlan = list()
        unknown_vlans = list()
        unknown_datastores = list()

        # counters for various conditions
        nb_templates = 0
        nb_machines = 0
        nb_skipped_machines = 0
        nb_skipped_guestos = 0
        nb_skipped_bad_datastore = 0
        nb_skipped_no_network = 0
        nb_skipped_too_many_nets = 0
        nb_skipped_no_network = 0
        nb_skipped_unknown_vlan = 0
        nb_skipped_no_net_info = 0
        nb_skipped_nb_addr_invalid = 0
        nb_saved_machines = 0
        nb_disks = 0

        # loop on virtual machines
        for path in sorted(virtual_machines):
            print('.', end="", flush=True)
            vm_data = virtual_machines[path]
            dir_path = vm_data['dir_path']
            v_machine = vm_data['vm']
            try:
                is_template = v_machine.config.template
            except:
                # this is used for debugging
                # 
                # pyVmomi.VmomiSupport.ManagedObjectNotFound: (vmodl.fault.ManagedObjectNotFound) {
                #    dynamicType = <unset>,
                #    dynamicProperty = (vmodl.DynamicProperty) [],
                #    msg = "The object 'vim.VirtualMachine:vm-62215' has already been deleted or has not been completely created",
                #    faultCause = <unset>,
                #    faultMessage = (vmodl.LocalizableMessage) [],
                #    obj = 'vim.VirtualMachine:vm-62215'
                # }
                print("problem attempting to get info on the machine ")
                continue

            # handle templates, attempt to add them to the templates table
            # note : a lot of templates are very similar, which leads to the
            # inability to import them
            if is_template:
                _vmc = v_machine.config
                print("guest_id     ", _vmc.guestId)
                print("name         ", _vmc.name)
                print("display_name ", _vmc.guestFullName)

                # create a VMGuestOs object
                try:
                    _guest_id = VMGuestOs.objects.get(name=v_machine.config.guestId)
                except VMGuestOs.DoesNotExist:
                    _guest_id = VMGuestOs()
                    _guest_id.name = _vmc.guestId
                    _guest_id.save()

                # find a template that matches the guest_id
                print("found guest_id ", _guest_id)
                try:
                    _template = Template.objects.get(name=v_machine.config.name)
                except Template.DoesNotExist:
                    _template = Template()
                print(_template)

                # update the template object if things have changed
                _save_template = False
                if _template.name != _vmc.name:
                    _template.name = _vmc.name
                    _save_template = True
                if _template.display_name != _vmc.guestFullName:
                    _template.display_name = _vmc.guestFullName
                    _save_template = True
                _update_guest_id = False
                try: 
                    if _template.guest_id != _guest_id:
                        _update_guest_id = True
                except:
                    _update_guest_id = True
                
                if _update_guest_id:
                    _template.guest_id = _guest_id
                    _save_template = True

                # save the template object
                if _save_template:
                    _template.save()

                nb_templates += 1
                continue

            nb_machines += 1
            
            _vm_name = v_machine.name

            if not v_machine.guest:
                print("unable to find guest in the virtual machine info")
                # print(v_machine)
                nb_skipped_machines += 1
                continue

            guest = v_machine.guest

            # attempt to find an appropriate template for the VM
            # note: may fail and return None
            if not guest.guestId:
                no_guestid.append(path)
                _vm_template = None
            else:
                # find the template with that guestId
                guest_id = guest.guestId
                try:
                    guest_os = VMGuestOs.objects.get(name=guest_id)
                except VMGuestOs.DoesNotExist:
                    unknown_guestos[path] = guest_id
                    # set template 
                    _vm_template = None
                else:
                    # now that we have the guest_os record, find the Template
                    try:
                        _vm_template = Template.objects.get(guest_id=guest_os)
                    except Template.DoesNotExist:
                        print("Template with guestId '%s' does not exist"%(guest_os))
                        _vm_template = None
                    except Template.MultipleObjectsReturned:
                        # ok, try to find with the guestFullName
                        try:
                            _vm_template = Template.objects.get(guest_id=guest_os, display_name=guest.guestFullName)
                        except Template.DoesNotExist:
                            print("template with guestId '%s' and display_name '%s' does not exist"%(guest_os, guest.guestFullName))
                            _vm_template = None
                        except Template.MultipleObjectsReturned:
                            print("multiple templates returned for guestId '%s' and display_name '%s'"%(guest_os, guest.guestFullName))
                            _vm_template = None
                        

            # vm_folder

            _vm_folder = dir_path

            # vm_datastore

            datastore = v_machine.datastore

            datastores = dict()
            for ds_entry in datastore:
                ds_entry_label = str(ds_entry)
                ds_info = ds_entry.info
                if not ds_info.name:
                    continue
                ds_name = ds_info.name
                datastores[ds_entry_label] = ds_name

            # find the datastore
            try:
                _vm_datastore = VMDatastore.objects.get(name=ds_name)
            except VMDatastore.DoesNotExist:
                _vm_datastore = VMDatastore()
                _vm_datastore.name = ds_name
                _vm_datastore.save()

            # hw_cpus
            hardware = v_machine.config.hardware
            _hw_cpus = hardware.numCPU

            # hw_memory
            _hw_memory = hardware.memoryMB

            # hw_root
            disks = []
            # vim.vm.device.VirtualDisk
            # add all disks to the list
            for device in hardware.device:
                if device.__class__.__name__ == "vim.vm.device.VirtualDisk":
                    # print(device)
                    backing = device.backing
                    datastore = backing.datastore
                    disk_data = dict()
                    disk_data['uuid'] = device.backing.uuid
                    disk_data['disk_id'] = device.diskObjectId
                    disk_data["index"] = device.unitNumber
                    disk_data["label"] = device.deviceInfo.label
                    disk_data['size_b'] = device.capacityInBytes
                    disk_data["size_gb"] = device.capacityInBytes / (1024 * 1024 * 1024)
                    disk_data["datastore"] = datastore
                    disk_data['datastore_label'] = datastore.name
                    
                    disks.append(disk_data)

            # we can do things with the list of disks (in each machine)
            # update disks table
            # note: we only handle a single disk for now
            # we may handle more in the future
            if len(disks) == 1:
                # should be a foreign key to one particular disk
                _hw_root = int(disks[0]["size_gb"])
            else:
                too_many_disks[path] = disks
                _hw_root = None

            # network stuff

            if not guest.net:
                no_network.append(path)
                nb_skipped_machines += 1
                nb_skipped_no_network += 1
                continue

            net = guest.net
            if len(net) != 1:
                valid_networks = []
                for nic_info in net:
                    # if vlan information is None, skip
                    if not nic_info.network:
                        continue
                    # if there is no ipAddress specified, skip
                    if not nic_info.ipAddress:
                        continue
                    valid_networks.append(nic_info)
                net = valid_networks
                if len(net) != 1:
                    too_many_networks[path] = net
                    nb_skipped_machines += 1
                    nb_skipped_too_many_nets += 1
                    continue

            net = net[0]

            # net_vlan

            if not net.network:
                unspecified_vlan.append(path)
                nb_skipped_machines += 1
                nb_skipped_no_network += 1
                continue

            vlan_name = net.network
            try:
                _net_vlan = Vlan.objects.get(name=vlan_name)
            except Vlan.DoesNotExist:
                _net_vlan = Vlan()
                _net_vlan.name = vlan_name
                _net_vlan.save()

            # get information about the machine's IP address
            # net_ip
            # net_pfx_len

            if net.ipAddress:
                ip_addresses = net.ipAddress
                # find the one that is an ipv4
                ip_addr_num = 0
                valid_ipv4 = dict()
                for ip_addr in ip_addresses:
                    try:
                        ip_addr_obj = ipaddress.ip_address(ip_addr)
                    except ValueError:
                        print("'%s' is not a valid ip address")
                    else:
                        if isinstance(ip_addr_obj, ipaddress.IPv4Address):
                            valid_ipv4[ip_addr_num] = ip_addr
                    # TODO: IPv6 should be taken into account...
                if len(valid_ipv4) != 1:
                    print("only one ipv4 expected, case not taken into account")
                    # print(valid_ipv4)
                    nb_skipped_machines += 1
                    nb_skipped_nb_addr_invalid += 1
                    continue

            # now, look into the ipAddress data structure
            if net.ipConfig:
                ip_config = net.ipConfig
                ip_addresses = ip_config.ipAddress
                ip_index = list(valid_ipv4.keys())[0]
                ip_address_data = ip_addresses[ip_index]
                # state should be "preferred"
                if ip_address_data.state != "preferred":
                    print("hmm, the ip address (%s) is not in the 'preferred' state"%(str(ip_address_data)))
                    nb_skipped_machines += 1
                    continue

                if not (ip_address_data.ipAddress or ip_address_data.prefixLength):
                    print("missing ipAddress and prefixLength in", ip_address_data)
                    nb_skipped_machines += 1
                    continue

                _net_ip = ip_address_data.ipAddress
                _net_pfx_len = int(ip_address_data.prefixLength)

                # guess the gateway from the IP address, as there is no way to 
                # identify the proper gateway address
                
                _net_addr = ipaddress.ip_network(_net_ip+"/"+str(_net_pfx_len), False)
                _net_gw = str(_net_addr[1])

                # net_hostname
                # obtain the PTR record from the DNS

                # by default, use the PTR record
                try:
                    host_data = socket.gethostbyaddr(_net_ip)
                except socket.herror as e:
                    print(e)
                    print("_net_ip : '%s'"%(str(_net_ip)))
                _net_hostname = host_data[0]  # hostname PTR record

                # now, if we have an IPAM server, use it to obtain the rest of the aliases
                # check if net_hostname is already set and corresponds to one of the known aliases
                try:
                    srv = IpamManager()
                except:
                    # there is no solid server available
                    _net_aliases = None
                else:
                    _net_aliases = srv.ip_addr_aliases(_net_ip)

            else:
                # this is when there is no network info
                _net_ip = None
                _net_pfx_len = None
                _net_addr = None
                _net_gw = None
                _net_hostname = ""
                _net_aliases = None


            # admin_user
            # there is no way to obtain info about the admin user at this stage


            # ------------------------------------------------------------------------------------------------
            #
            # WARNING: vm_object is only valid after this !
            # 

            # everything is ok. attempt to save

            try:
                vm_object = Machine.objects.get(vm_name=v_machine.name)
                is_new = False
            except Machine.DoesNotExist:
                # no machine with that name, create a new machine object
                vm_object = Machine()
                is_new = True

            # function to update one variable of the vm object, and return if things were changed
            def update(attr, value, _messages=None, _vm_object=vm_object, _is_new=is_new):
                old_value = getattr(_vm_object, attr)
                if _is_new or old_value != value:
                    _messages.append("+ '%s' : %s"%(attr, str(value)))
                    setattr(_vm_object, attr, value)
                    return True
                _messages.append(". '%s' : %s"%(attr, str(value)))
                return False

            # have we changed anything ?
            changed = False
            # change messages (start with + if a particular variable was changed)
            messages = list()

            #========================================================================================

            # do the changes

            # only change the template if it is not None...
            # there appears to be no way to guess the template from a machine
            if _vm_template:
                changed |= update("vm_template", _vm_template, messages)
            changed |= update("vm_folder", _vm_folder, messages)
            changed |= update("vm_datastore", _vm_datastore, messages)
            changed |= update("vm_name", _vm_name, messages)
            changed |= update("hw_cpus", _hw_cpus, messages)
            changed |= update("hw_memory", _hw_memory, messages)
            changed |= update("hw_root", _hw_root, messages)
            changed |= update("net_vlan", _net_vlan, messages)

            if _net_aliases:      
                print(_net_aliases)
                for alias in _net_aliases:
                    if vm_object.net_hostname == alias:
                        print("setting _net_hostname to", alias)
                        _net_hostname = alias
                        break
            changed |= update("net_hostname", _net_hostname, messages)

            # generate search domains
            _net_search_domains = vm_object.net_search_domains
            print("search domains: ", vm_object.net_search_domains)
            if (_net_hostname is not None) and (_net_search_domains is None or not _net_search_domains.strip()):
                domain_bits = _net_hostname.split('.')
                print(domain_bits)
                if len(domain_bits) > 1:
                    _net_search_domains = '.'.join(domain_bits[1:])
            else:
                # TODO: generate a search domain if we have a valid hostname or alias list
                _net_search_domains = ""

            changed |= update("net_search_domains", _net_search_domains, messages)

            changed |= update("net_ip", _net_ip, messages)
            changed |= update("net_pfx_len", _net_pfx_len, messages)
            changed |= update("net_gw", _net_gw, messages)

            # save the vm_object if things were changed
            if changed:
                print("\n".join(messages))
                vm_object.save()
            nb_saved_machines += 1

            #
            # add all disks found to the vm_disks table.
            # add the datastore too
            # 
            for disk in disks:
                # print(disk)
                datastore_name = disk['datastore_label']
                try:
                    disk_datastore = VMDatastore.objects.get(name=datastore_name)
                except VMDatastore.DoesNotExist:
                    print("\ncreating datastore %s"%(datastore_name))
                    disk_datastore = VMDatastore()
                    disk_datastore.name = datastore_name
                    disk_datastore.save()
                
                # important : disk index is NOT a key

                # disk should be idendified by uuid then
                vm_disks = VMDisk.objects.filter(datastore=disk_datastore, disk_id=disk['disk_id'])
                # print("found VMDisk %s"%(str(vm_disks)))
                disk_changed = False
                # print(len(vm_disks))
                disk_size_b = disk['size_b']
                if len(vm_disks) == 0:
                    # time to create a new disk
                    vm_disk = VMDisk()
                    vm_disk.datastore = disk_datastore
                    vm_disk.vm = vm_object
                    vm_disk.uuid = disk['uuid']
                    vm_disk.disk_id = disk['disk_id']
                    vm_disk.index = disk['index']
                    vm_disk.size_b = disk_size_b
                    vm_disk.label = disk['label']
                    disk_changed = True
                elif len(vm_disks) != 1:
                    print()
                    print("------------------------------------------------------------")
                    print("ERROR: something wierd here, we should have only one disk matching the request")
                    print(vm_disks)
                    print("------------------------------------------------------------")
                    continue
                else:
                    vm_disk = vm_disks[0] 
                    # print("\nvm_disk already exists %s %s"%(vm_disk, str(vm_disk.size_gb)))
                    if vm_disk.size_b != disk_size_b:
                        print("disk size changed from ", vm_disk.size_b, ' to ', disk_size_b)
                        vm_disk.size_b = disk_size_b
                        disk_changed = True
                    if vm_disk.label != disk['label']:    
                        vm_disk.label = disk['label']
                        disk_changed = True
                if disk_changed:
                    print()
                    print('===========================================================')
                    print("saving disk %s"%(str(disk)))
                    print("vm: %s"%(str(vm_object)))
                    print()
                    vm_disk.save()
                    # print("size: %s"%(str(vm_disk.size_gb)))
                nb_disks += 1


        #
        # end of process, output a series of statistics
        #

        print()
        # when done, list issues
        print("VMs with no guestId :")
        no_guestid.sort()
        for v_machine in no_guestid:
            print("    * %s"%(v_machine))

        print("VMs with unknown os :")
        v_machines = list(unknown_guestos)
        v_machines.sort()
        for v_machine in v_machines:
            print("    * %s : %s"%(v_machine, unknown_guestos[v_machine]))

        print("VMs with too many disks :")
        v_machines = list(too_many_disks)
        v_machines.sort()
        for v_machine in v_machines:
            print("    * %s :"%(v_machine))
            for disk in too_many_disks[v_machine]:
                print("        - %s"%(str(disk)))

        if no_network:
            print("VMs with no network specified :")
            no_network.sort()
            for v_machine in no_network:
                print("    * %s"%(v_machine))

        if too_many_networks:
            print("VMs with too many networks specified :")
            v_machines = list(too_many_networks)
            v_machines.sort()
            for v_machine in v_machines:
                print("    * %s"%(v_machine))
                for nic_info in too_many_networks[v_machine]:
                    ip_address = "[]"
                    if nic_info.ipAddress:
                        ip_address = "[ " + ", ".join(nic_info.ipAddress) + " ]"
                    print("         - %s %s %s"%
                          (str(nic_info.network),
                           ip_address,
                           str(nic_info.macAddress)))
                # print(too_many_networks[v_machine])

        print("VMs with unspecified vlan :")
        unspecified_vlan.sort()
        for v_machine in unspecified_vlan:
            print("     * %s"%(v_machine))

        if unknown_vlans:
            print("Unknown Vlans :")
            unknown_vlans.sort()
            for vlan in unknown_vlans:
                print("     * %s"%(vlan))

        print("unknown datastores :")
        unknown_datastores.sort()
        for datastore in unknown_datastores:
            print("    * %s"%(datastore))

        print("numbers :")
        print("    * templates         : %d"%(nb_templates))
        print("    * total machines    : %d"%(nb_machines))
        print("    * skipped machines  : %d"%(nb_skipped_machines))
        print("    * unknown os        : %d"%(nb_skipped_guestos))
        print("    * unknown datastore : %d"%(nb_skipped_bad_datastore))
        print("    * no network        : %d"%(nb_skipped_no_network))
        print("    * too many nets     : %d"%(nb_skipped_too_many_nets))
        print("    * unknown vlan      : %d"%(nb_skipped_unknown_vlan))
        print("    * no network info   : %d"%(nb_skipped_no_net_info))
        print("    * nb addr invalid   : %d"%(nb_skipped_nb_addr_invalid))
        print("    * saved machines    : %d"%(nb_saved_machines))
        print("    * disks             : %d"%(nb_disks))
