"""
Tests the IPAM module
"""

from django.core.management.base import BaseCommand
from siosug.backends.ipam import IpamManager

class Command(BaseCommand):
    """ Django admin command to test the IPAM module """

    def handle(self, *args, **options):
        ipam = IpamManager()
        print(ipam.ip_addr_aliases('129.88.191.55'))
