from django.core.management.base import BaseCommand  # , CommandError

from ...models import Machine, Template, Vlan, VMDatastore, VMDisk, VMGuestOs

GIGABYTE = 1024*1024*1024


class Command(BaseCommand):
    """
    Command object for the vsphere_import_vms command
    """
    def handle(self, *args, **options):
        disks = VMDisk.objects.all().order_by('vm', 'disk_id')
        for disk in disks:
            print("'%s','%s','%s',%d,%f,'%s','%s'"%(disk.vm, disk.label, disk.datastore, disk.index, disk.size_b/GIGABYTE, disk.disk_id, disk.uuid))
                