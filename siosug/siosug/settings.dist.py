#======================================================================================
# 
# Configurations des variables nécessaires à Django
#
# pour les variables spécifiques a SiOSUG, voir plus bas
#
#======================================================================================

import os

#
# configuration des répertoires et du fichiers de logins ansible
# 
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

#
# clé sercrete pour les cookies.
# générer avec 
#
# pwgen -y -s 64 1 
#
# attention aux "'" dans le mot de passe généré (escape nécessaire)
#

SECRET_KEY = ''

# 
# ATTENTION: en production, cette variable doit etre False
#
DEBUG = True

#
# liste des machines ayant le droit de se connecter à l'application Django
# liste vide = toute machine
#

ALLOWED_HOSTS = []

#
# module d'authentification
#

AUTHENTICATION_BACKENDS = ['siosug.backends.OsugAuthenticationBackend']
AUTH_USER_MODEL = "siosug.User"

# adresse du serveur LDAP sur lequel faire l'authentification

OSUG_LDAP_SERVER = 'osug-ldap-slave.ujf-grenoble.fr'

#
# configuration des loggers
#

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[%(asctime)s] %(message)s'
        },
        'django.server': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '[{asctime}] {message}',
            'style': '{'
        }
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'django.server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'django.server',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['console']
        },
        'django.server': {
            'handlers': ['django.server']
        },
        'django.db.backends': {
            'handlers': ['console']
        },
        'OSUG.User': {
            'handlers': ['console']
        },
        'OsugAuthenticationBackend': {
            'handlers': ['console']
        }
    }
}

#
# Définition des applications Django installées
#

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_archive',
    'siosug',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
#    'django_currentuser.middleware.ThreadLocalUserMiddleware',
#    'siosug.middleware.OsugCurrentUser',
]

ROOT_URLCONF = 'siosug.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'siosug.context_processors.siosug_vars.siosug_vars',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'siosug.wsgi.application'


# Configuration de la base de données

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}


# Validateurs de mots de passe 
# (tests sur la qualité des mots de passe)
# l'utilité de ces modules n'est pas encore établie

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.'
                'UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.'
                'MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.'
                'CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.'
                'NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# répertoire ou sont stockées les données statiques

STATIC_URL = '/static/'



#==================================================================================================
#
# Variables spécifiques a l'application SiOsug
#
#==================================================================================================

APP_NAME = 'SiOSUG'

# pointeurs vers les repertoires ou sont les scripts Ansible
ANSIBLE_DIR = os.path.join(BASE_DIR, 'ansible')
ANSIBLE_VARS_DIR = os.path.join(ANSIBLE_DIR, 'vars')
ANSIBLE_LOGINS = os.path.join(ANSIBLE_VARS_DIR, 'logins.yml')

#
# adresse du repository des anciens modules Ansible avec leur configuration
#

OLD_ANSIBLE_REPO = "git@gricad-gitlab.univ-grenoble-alpes.fr:OSUG/DC/toolbox.git"
#OLD_ANSIBLE_REPO = "https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/DC/toolbox.git"

#
# variables de configuration du module IPAM ( IP Address Management - Gestionnaire de DNS )
# si vous n'avez pas d'IPAM, commentez la section.
#

IPAM = {
    'ENGINE'  : 'siosug.backends.ipam.EfficientIP.SolidServer',
    'HOST'    : '',
    'USER'    : '',
    'PASSWORD': ''
}
