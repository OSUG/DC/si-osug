#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import sys
import urllib.request
import os

def get_inventory():
    dirpath = os.path.dirname(os.getcwd())
    sys.path.insert(0, dirpath)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "siosug.settings")
    from django.conf import settings
    req = urllib.request.urlopen(settings.INVENTORY_URL)
    data = req.read()
    return data.decode('utf-8')

if __name__ == "__main__":
    # print(str(sys.argv), file=sys.stderr)
    inventory = get_inventory()
    print(inventory)
