#!/usr/bin/python

import ipaddress
import os.path
import socket

from ansible.module_utils.basic import AnsibleModule

# Copyright: (c) 2019, Raphaël Jacquot <raphael.jacquot@univ-grenoble-alpes.fr>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'certified'
}



class SshKeyModule(AnsibleModule):
    def __init__(self):
        module_args = dict(
            host=dict(type='str', required=True)
        )

        super().__init__(argument_spec=module_args, supports_check_mode=True)

    def get_ip_addresses(self, host):
        data = socket.getaddrinfo(host, None)
        addr_list = list()
        for d in data:
            family, s_type, proto, name, sockaddr = d
            # address component is index 0
            addr = sockaddr[0]
            if addr not in addr_list:
                addr_list.append(addr)
        return addr_list 

    def parse_key_list(self, keys):
        lines = keys.split('\n')
        keylist = {}
        for l in lines:
            if not l or l[0] == '#':
                continue
            host, keytype, keydata = l.split(' ')
            keylist[keytype]= keydata
        return keylist

    def get_current_keys(self, host, ip):
        """
        get known keys from the local storage 
        """
        rc, stdout, stderr = self.run_command(['ssh-keygen', '-F', host])
        host_keys = self.parse_key_list(stdout)
        rc, stdout, stderr = self.run_command(['ssh-keygen', '-F', str(ip)])
        ip_keys = self.parse_key_list(stdout)
        # merge both lists
        for k in ip_keys:
            if k not in host_keys:
                host_keys[k] = ip_keys[k]
        return host_keys

    def get_machine_keys(self, host, ip):
        """
        get known keys from the machine itself
        """
        rc, stdout, stderr = self.run_command(['ssh-keyscan', host])
        host_keys = self.parse_key_list(stdout)
        rc, stdout, stderr = self.run_command(['ssh-keyscan', str(ip)])
        ip_keys = self.parse_key_list(stdout)
        # merge both lists
        for k in ip_keys:
            if k not in host_keys:
                host_keys[k] = ip_keys[k]
            else:
                # we already have a key with this spec...
                pass
        return host_keys

    def remove_all_keys(self, host, ip):
        """
        remove all keys for host in known_hosts
        """
        rc, _, _ = self.run_command(['ssh-keygen', '-R', host])
        if rc != 0:
            return False
        rc, _, _ = self.run_command(['ssh-keygen', '-R', str(ip)])
        if rc != 0:
            return False
        return True

    def format_key(self, host, ip, keytype, keyvalue):
        hoststr = ','.join([host, str(ip)])
        return ' '.join([hoststr, keytype, keyvalue])

    def add_keys(self, host, ip, keys):
        # append to the end of the file
        f = open(os.path.expanduser("~/.ssh/known_hosts"), "a")
        for key in keys:
            f.write(self.format_key(host, ip, key, keys[key])+'\n')
        f.close()

    def run(self):
        
        result = dict(
            changed=False,
            message=''
        )

        # 1. analyse 'host', see if it's a valid name/ip
        host = self.params['host']
        result['message'] = "found host '%s'"%host

        # 2. obtain the other info (name/ip)
        try:
            ip = ipaddress.ip_address(host)
        except ValueError:
            # probably a dns name - attempt to find ips
            ip_addresses = self.get_ip_addresses(host)
            # get the first one for now
            ip = ipaddress.ip_address(ip_addresses[0])
            
            pass
        else:
            # either IPv4 or IPv6 - find the name
            pass

        result['ip'] = str(ip)

        # 3. list current keys for name and ip
        stored_keys = self.get_current_keys(host, ip)
        result['stored_keys'] = stored_keys

        # 4. obtain current machine's keys for name and ip
        machine_keys = self.get_machine_keys(host, ip)
        result["machine_keys"] = machine_keys

        # 5. compare keys, determine if things need to be changed
        msg=[]
        change = False

        if len(machine_keys) != len(stored_keys):
            change = True
        else:
            for k1type in machine_keys:
                msg.append("k1type '%s'"%(k1type))
                if k1type not in stored_keys:
                    change=True
                    msg.append("type not in stored_keys")
                    break
                if machine_keys[k1type] != stored_keys[k1type]:
                    change=True
                    msg.append("machine_key and stored_key differ")
                    break
                

        result['change_msg'] = msg
        
        if change:
            result['changed'] = change

            # if we are here to actually do things...
            if not self.check_mode:
        # 6.change.1. remove all known keys
                if not self.remove_all_keys(host, ip):
                    # an error was detected
                    pass 
        # 6.change.2. add all keys found
                self.add_keys(host, ip, machine_keys)
        # 7. return
        self.exit_json(**result)

def main():
    SshKeyModule().run()

if __name__ == '__main__':
    main()
